package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.AnimalDB
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class Animal(var id: Long, var name: String, var number: String, var variety: String, var born_at: Date, var abilities: MutableList<String>) : InterItem {

    companion object {
        fun createFromDB(a: AnimalDB): Animal {
            return Animal(a.a_id, a.name, a.number, a.variety, Date(a.born_at), a.abilities.split(", ").toMutableList())
        }
    }

    fun adaptForDb(): AnimalDB {
        return AnimalDB(id, name, number, variety, born_at.time, abilities.joinToString())
    }

    override fun getProductId(): Long { return this.id }
    override fun getProductName(): String { return this.name }
    override fun getItemVariety(): String? { return this.variety }
    override fun getItemAbilities(): List<String> { return this.abilities }
    override fun getStartDate(): Date? { return this.born_at }
    override fun getEndDate(): Date? { return null }

    override fun setQuantity(q: Double?, unit: String?) {}
    override fun getQuantity(): Double? { return null }
    override fun getUnit(): String? { return null }
    override fun getHours(): Double? { return null }
    override fun getDerivative(): String? { return null }
    override fun getSurfaceArea(): NetSurfaceArea? { return null }
    override fun getSvgShape(): String? { return null }
}