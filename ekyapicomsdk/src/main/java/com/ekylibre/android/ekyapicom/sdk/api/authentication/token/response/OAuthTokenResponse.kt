package com.ekylibre.android.ekyapicom.sdk.api.authentication.token.response

import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class OAuthTokenResponse(var access_token: String, var expires_in: Int, var token_type: String, var refresh_token: String): APIResponse {}