package com.ekylibre.android.ekyapicom.sdk.database.dao.procedures

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureProductDB

@Dao
interface ProcedureProductDAO {
    @Query("SELECT * FROM procedureproductdb")
    fun getAll(): List<ProcedureProductDB>

    @Query("SELECT * FROM procedureproductdb WHERE procedure_parameter_foreign_id = :paramId")
    fun getByParameterId(paramId: Long): List<ProcedureProductDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(procedureProductDB: ProcedureProductDB): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg procedureProductDB: ProcedureProductDB): List<Long>

    @Query("DELETE FROM procedureproductdb")
    fun deleteAll()
}