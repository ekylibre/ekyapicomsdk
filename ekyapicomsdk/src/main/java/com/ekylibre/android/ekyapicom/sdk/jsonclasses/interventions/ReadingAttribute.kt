package com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ReadingAttribute(var indicator_name: String, var measure_value_value: Double?, var measure_value_unit: String?, var choice_value: String?, var boolean_value: Boolean?) {


    companion object {
        fun constructForAPI(indicator: String, value: Double?, unit: String): ReadingAttribute? {
            value?.let { return ReadingAttribute(indicator, value, unit, null, null) }
            return null
        }

        fun constructForAPI(indicator: String, value: String?): ReadingAttribute? {
            value?.let { return ReadingAttribute(indicator, null, null, value, null) }
            return null
        }

        fun constructForAPI(indicator: String, value: Boolean?): ReadingAttribute? {
            value?.let { return ReadingAttribute(indicator, null, null, null, value) }
            return null
        }
    }
}
