package com.ekylibre.android.ekyapicom.sdk.database.dao.lexicon

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.PhytoUsageDB

@Dao
interface PhytoUsageDAO {
    @Query("SELECT * FROM phytousagedb")
    fun getAll(): List<PhytoUsageDB>

    @Query("SELECT * FROM phytousagedb WHERE usage_eky_id IN (:ids)")
    fun loadAllByIds(ids: Array<String>): List<PhytoUsageDB>

    @Query("SELECT * FROM phytousagedb WHERE usage_product_id = :id")
    fun loadAllByProductId(id: Long): List<PhytoUsageDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg u: PhytoUsageDB)

    @Delete
    fun delete(u: PhytoUsageDB)
}