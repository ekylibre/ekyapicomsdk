package com.ekylibre.android.ekyapicom.sdk.database.dao.procedures

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureHandlerDB

@Dao
interface ProcedureHandlerDAO {
    @Query("SELECT * FROM procedurehandlerdb")
    fun getAll(): List<ProcedureHandlerDB>

    @Query("SELECT * FROM procedurehandlerdb WHERE procedure_product_parameter_foreign_id = :productId")
    fun getByProductId(productId: Long): List<ProcedureHandlerDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(procedureHandlerDB: ProcedureHandlerDB): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg procedureHandlerDB: ProcedureHandlerDB): List<Long>

    @Query("DELETE FROM procedurehandlerdb")
    fun deleteAll()
}