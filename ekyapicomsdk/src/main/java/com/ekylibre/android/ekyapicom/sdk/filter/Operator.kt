package com.ekylibre.android.ekyapicom.sdk.filter

enum class Operator {
    OR,
    AND
}