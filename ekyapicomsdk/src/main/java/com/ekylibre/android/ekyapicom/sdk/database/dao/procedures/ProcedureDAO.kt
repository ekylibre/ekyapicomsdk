package com.ekylibre.android.ekyapicom.sdk.database.dao.procedures

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureDB

@Dao
interface ProcedureDAO {
    @Query("SELECT * FROM proceduredb")
    fun getAll(): List<ProcedureDB>

    @Query("SELECT proc_categories FROM proceduredb")
    fun getProcedureCategories(): List<String>

    @Query("SELECT * FROM proceduredb WHERE proc_name LIKE :procedureName")
    fun getProcedureByName(procedureName: String): List<ProcedureDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(procedureDB: ProcedureDB): Long

    @Query("DELETE FROM proceduredb")
    fun deleteAll()
}