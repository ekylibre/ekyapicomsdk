package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.LandParcelDB
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class LandParcel(var id: Long, var name: String, var net_surface_area: NetSurfaceArea, var born_at: Date, var dead_at: Date?, var variety: String, var production_started_on: String?, var production_stopped_on: String?, var shape_svg: String?): InterItem {
    fun adaptForDb(): LandParcelDB {
        return LandParcelDB(id, name, net_surface_area.prepareForDB(), born_at.time, if (dead_at != null) dead_at!!.time else -1, variety, if (production_started_on != null) production_started_on!! else "", if (production_stopped_on != null) production_stopped_on!! else "", shape_svg ?: "")
    }

    companion object {
        fun createFromLandParcelDB(lp: LandParcelDB): LandParcel {
            return LandParcel(lp.lp_id, lp.name, NetSurfaceArea(lp.netSurfaceArea), Date(lp.born_at), Date(lp.dead_at), lp.variety, lp.productionStartedOn, lp.productionStopped, lp.shapeSvg)
        }
    }

    override fun getProductId(): Long { return this.id }
    override fun getProductName(): String { return this.name }
    override fun setQuantity(q: Double?, unit: String?) {}
    override fun getQuantity(): Double? { return null }
    override fun getUnit(): String? { return null }
    override fun getHours(): Double? { return null }
    override fun getItemVariety(): String { return this.variety }
    override fun getItemAbilities(): List<String> { return emptyList() }
    override fun getStartDate(): Date? { return this.born_at }
    override fun getEndDate(): Date? { return this.dead_at }
    override fun getDerivative(): String? { return null }
    override fun getSurfaceArea(): NetSurfaceArea? { return this.net_surface_area }
    override fun getSvgShape(): String? { return this.shape_svg }
}