package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.BuildingDivisionDB
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class BuildingDivision(var id: Long, var name: String, var net_surface_area: NetSurfaceArea, var born_at: Date, var variety: String): InterItem {
    fun adaptForDb(): BuildingDivisionDB {
        return BuildingDivisionDB(id, name, net_surface_area.prepareForDB(), born_at.time, variety)
    }

    companion object {
        fun createFromBuildingDivisionDB(bd: BuildingDivisionDB): BuildingDivision {
            return BuildingDivision(bd.bd_id, bd.name, NetSurfaceArea(bd.net_surface_area), Date(bd.born_at), bd.variety)
        }
    }

    override fun getProductId(): Long { return this.id }
    override fun getProductName(): String { return this.name }
    override fun setQuantity(q: Double?, unit: String?) {}
    override fun getQuantity(): Double? { return null }
    override fun getUnit(): String? { return null }
    override fun getHours(): Double? { return null }
    override fun getItemVariety(): String { return this.variety }
    override fun getItemAbilities(): List<String> { return ArrayList() }
    override fun getStartDate(): Date? { return this.born_at }
    override fun getEndDate(): Date? { return null }
    override fun getDerivative(): String? { return null }
    override fun getSurfaceArea(): NetSurfaceArea? { return null }
    override fun getSvgShape(): String? { return null }
}