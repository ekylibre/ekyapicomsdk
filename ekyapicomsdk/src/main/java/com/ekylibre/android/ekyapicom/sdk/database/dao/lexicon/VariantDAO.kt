package com.ekylibre.android.ekyapicom.sdk.database.dao.lexicon

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.VariantDB

@Dao
interface VariantDAO {
    @Query("SELECT * FROM variantdb")
    fun getAll(): List<VariantDB>

    @Query("SELECT * FROM variantdb WHERE variant_variety LIKE :variety")
    fun getByVariety(variety: String): List<VariantDB>

    @Query("SELECT * FROM variantdb WHERE variant_id IN (:variantIds)")
    fun loadAllByIds(variantIds: LongArray): List<VariantDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg variantDB: VariantDB)

    @Delete
    fun delete(variantDB: VariantDB)
}