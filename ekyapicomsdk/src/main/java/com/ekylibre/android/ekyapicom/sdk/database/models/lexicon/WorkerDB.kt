package com.ekylibre.android.ekyapicom.sdk.database.models.lexicon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class WorkerDB (
    @PrimaryKey val w_id: Long,
    @ColumnInfo(name = "w_name") val name: String,
    @ColumnInfo(name = "w_number") val number: String,
    @ColumnInfo(name = "w_work_number") val work_number: String,
    @ColumnInfo(name = "w_born_at") val born_at: Long,
    @ColumnInfo(name = "w_variety") val variety: String,
    @ColumnInfo(name = "w_abilities") val abilities: String
)