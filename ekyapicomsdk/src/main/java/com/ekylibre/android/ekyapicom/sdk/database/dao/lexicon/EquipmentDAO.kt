package com.ekylibre.android.ekyapicom.sdk.database.dao.lexicon

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.EquipmentDB
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.LandParcelDB

@Dao
interface EquipmentDAO {
    @Query("SELECT * FROM equipmentdb")
    fun getAll(): List<EquipmentDB>

    @Query("SELECT * FROM equipmentdb WHERE e_variety LIKE :variety")
    fun getbyVariety(variety: String): List<EquipmentDB>

    @Query("SELECT * FROM equipmentdb WHERE e_id IN (:equipmentIds)")
    fun loadAllByIds(equipmentIds: LongArray): List<EquipmentDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg equipmentDB: EquipmentDB)

    @Delete
    fun delete(workerDB: EquipmentDB)
}