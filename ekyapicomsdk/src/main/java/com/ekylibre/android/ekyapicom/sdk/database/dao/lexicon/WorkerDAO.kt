package com.ekylibre.android.ekyapicom.sdk.database.dao.lexicon

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.MatterDB
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.WorkerDB

@Dao
interface WorkerDAO {
    @Query("SELECT * FROM workerdb")
    fun getAll(): List<WorkerDB>

    @Query("SELECT * FROM workerdb WHERE w_variety LIKE :variety")
    fun getByVariety(variety: String): List<WorkerDB>

    @Query("SELECT * FROM workerdb WHERE w_id IN (:workerIds)")
    fun loadAllByIds(workerIds: LongArray): List<WorkerDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg workerDB: WorkerDB)

    @Delete
    fun delete(workerDB: WorkerDB)
}