package com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PaginatedPhytoResponse(var pagination: Pagination?, var data: List<Phyto>) {
}