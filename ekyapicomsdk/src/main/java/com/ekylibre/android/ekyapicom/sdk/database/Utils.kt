package com.ekylibre.android.ekyapicom.sdk.database

import android.content.Context
import android.util.Log

class Utils {

    companion object {
        fun saveInSharedPreferences(keysValues: Map<String, String>, context: Context) {
            val sharedPreferences = context.getSharedPreferences(context.packageName, 0)
            val prefsEditor = sharedPreferences!!.edit()

            for ((k, v) in keysValues) {
                prefsEditor.putString(k, v)
                Log.e(
                    "EkySDK",
                    "Saved value " + v + " in " + context.packageName + " under key " + k
                )
            }

            prefsEditor.apply()
        }

        fun readStringInSharedPreferences(key: String, context: Context): String {
            val sharedPreferences = context.getSharedPreferences(context.packageName, 0)
            return sharedPreferences!!.getString(key, "No value stored corresponding to this key")!!
        }
    }
}