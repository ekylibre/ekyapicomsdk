package com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class Pagination(var elements: Long, var page: Long, var total_elements: Long, var page_count: Long) {
}