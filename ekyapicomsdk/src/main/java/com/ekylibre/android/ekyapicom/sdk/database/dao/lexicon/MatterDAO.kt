package com.ekylibre.android.ekyapicom.sdk.database.dao.lexicon

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.BuildingDivisionDB
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.MatterDB

@Dao
interface MatterDAO {
    @Query("SELECT * FROM matterdb")
    fun getAll(): List<MatterDB>

    @Query("SELECT * FROM matterdb WHERE m_variety LIKE :variety")
    fun getByVariety(variety: String): List<MatterDB>

    @Query("SELECT * FROM matterdb WHERE m_id IN (:matterIds)")
    fun loadAllByIds(matterIds: LongArray): List<MatterDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg matterDB: MatterDB)

    @Delete
    fun delete(matterDB: MatterDB)
}