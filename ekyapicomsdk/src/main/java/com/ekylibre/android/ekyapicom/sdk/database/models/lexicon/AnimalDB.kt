package com.ekylibre.android.ekyapicom.sdk.database.models.lexicon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AnimalDB (
    @PrimaryKey
    val a_id: Long,
    @ColumnInfo(name = "a_name") val name: String,
    @ColumnInfo(name = "a_number") val number: String,
    @ColumnInfo(name = "a_variety") val variety: String,
    @ColumnInfo(name = "a_born_at") val born_at: Long,
    @ColumnInfo(name = "a_abilities") val abilities: String
    )