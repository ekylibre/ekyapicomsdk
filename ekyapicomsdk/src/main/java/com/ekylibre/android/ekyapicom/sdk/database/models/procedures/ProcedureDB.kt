package com.ekylibre.android.ekyapicom.sdk.database.models.procedures

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ProcedureDB (
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "proc_id") val id: Long = 0,
        @ColumnInfo(name = "proc_name") val name: String,
        @ColumnInfo(name = "proc_categories") val categories: String,
        @ColumnInfo(name = "proc_position") val position: String
) {
    constructor(name: String, categories: String, position: String) : this(0, name, categories, position)
}