package com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions

import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionParameterDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionParameterWithProductDB
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class Parameter(var id: Long, var role: String, var name: String, var label: String, var product_id: Long) {

    fun adaptForDb(interId: Long): InterventionParameterDB {
        return InterventionParameterDB(id, role, name, label, product_id, interId)
    }

    override fun toString(): String {
        return "Parameter: ${id}, role: ${role}, name: ${name}, label: ${label}, product: $product_id"
    }

    companion object {
        fun createFromParameterDB(p: InterventionParameterDB): Parameter {
            return Parameter(p.p_id, p.role, p.name, p.label, p.productId)
        }
    }
}