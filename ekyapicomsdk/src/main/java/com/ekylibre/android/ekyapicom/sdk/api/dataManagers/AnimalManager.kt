package com.ekylibre.android.ekyapicom.sdk.api.dataManagers

import android.content.Context
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.api.APICommunicationModule
import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.ekylibre.android.ekyapicom.sdk.api.ApiInterface
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.Animal
import retrofit2.Call
import retrofit2.Response

class AnimalManager(var context: Context, var ekyDbSDK: EkyDbSDK): APICommunicationModule {

    override val errorSharedPrefsKey: String
        get() = "sync_error"

    override fun onResponseCallback(r: Response<APIResponse>) {
        if (r.body() != null && r.isSuccessful) {
            val l: List<Animal> = r.body()!! as List<Animal>
            ekyDbSDK.storeAnimal(l.map { it.adaptForDb() })

            Utils.saveInSharedPreferences(mapOf("last_animals_sync" to Utils.getTodaysDate()), context)
        }
    }

    fun pull() {
        val modifiedSince = Utils.readStringInSharedPreferences("last_animals_sync", context)
        val response = if (modifiedSince == "") ApiInterface.get(context)?.getAnimalsAsync(null) else ApiInterface.get(context)?.getAnimalsAsync(modifiedSince)
        val apiUrl = Utils.readStringInSharedPreferences("api_url", context)
        response?.let { enqueueResponse(apiUrl, response as Call<APIResponse>, context) }
    }
}