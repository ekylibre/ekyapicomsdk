package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ResponseInterventionId(var id: Long): APIResponse {
}