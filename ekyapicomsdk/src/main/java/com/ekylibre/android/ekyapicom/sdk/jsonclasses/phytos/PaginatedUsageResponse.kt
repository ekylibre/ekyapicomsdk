package com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PaginatedUsageResponse(var pagination: Pagination?, var data: List<PhytoUsage>) {
}