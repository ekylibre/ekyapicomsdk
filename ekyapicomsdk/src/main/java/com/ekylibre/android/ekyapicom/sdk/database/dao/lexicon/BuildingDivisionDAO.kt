package com.ekylibre.android.ekyapicom.sdk.database.dao.lexicon

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.BuildingDivisionDB
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.LandParcelDB

@Dao
interface BuildingDivisionDAO {
    @Query("SELECT * FROM buildingdivisiondb")
    fun getAll(): List<BuildingDivisionDB>

    @Query("SELECT * FROM buildingdivisiondb WHERE bd_variety LIKE :variety")
    fun getByVariety(variety: String): List<BuildingDivisionDB>

    @Query("SELECT * FROM buildingdivisiondb WHERE bd_id IN (:buildingDivisionIds)")
    fun loadAllByIds(buildingDivisionIds: LongArray): List<BuildingDivisionDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg buildingDivisionDB: BuildingDivisionDB)

    @Delete
    fun delete(buildingDivisionDB: BuildingDivisionDB)
}