package com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos

import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.PhytoUsageDB
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PhytoUsage(var id: String, var product_id: Long, var ephy_usage_phrase: String, var crop: Translations, var species: MutableList<String>, var target_name: Translations, var description: String?, var treatment: Translations?, var dose_quantity: String?,
    var dose_unit: String?, var dose_unit_name: String?, var dose_unit_factor: Double?, var pre_harvest_delay: Long, var pre_harvest_delay_bbch: Long?, var applications_count: Long?, var applications_frequency: Long?, var development_stage_min: Long?, var development_stage_max: Long?,
    var usage_conditions: String?, var untreated_buffer_aquatic: Long?, var untreated_buffer_arthropod: Long?, var untreated_buffer_plants: Long?, var decision_date: String?, var state: String, var record_checksum: Long) {

    fun adaptForDb(): PhytoUsageDB {
        return PhytoUsageDB(id, product_id, ephy_usage_phrase, species.joinToString(","), target_name.fra, description ?: "", treatment?.fra ?: "", dose_quantity ?: "", dose_unit ?: "", dose_unit_name ?: "", dose_unit_factor ?: .0, pre_harvest_delay, pre_harvest_delay_bbch ?: 0,
            applications_count ?: 0, applications_frequency ?: 0, development_stage_min ?: 0, development_stage_max ?: 0, usage_conditions ?: "", untreated_buffer_aquatic ?: 0, untreated_buffer_arthropod ?: 0, untreated_buffer_plants ?: 0, decision_date ?: "", state)
    }

    companion object {
        fun createFromPhytoUsageDB(u: PhytoUsageDB): PhytoUsage {
            return PhytoUsage(u.ekyId, u.productId, u.ephyUsagePhrase, Translations(""), u.species.split(",").toMutableList(), Translations(u.targetName), u.description, Translations(u.treatment), u.doseQuantity, u.doseUnit, u.doseUnitName, u.doseUnitFactor,
                u.preHarvestDelay, u.preHarvestDelayBBCH, u.applicationsCount, u.applicationsFrequency, u.developmentStageMin, u.developmentStageMax, null, u.untreatedBufferAquatic, u.untreatedBufferArthropod, u.untreatedBufferPlants, u.decisionDate, u.state, 0)
        }
    }
}