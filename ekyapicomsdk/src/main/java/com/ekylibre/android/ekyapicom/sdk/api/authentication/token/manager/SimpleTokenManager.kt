package com.ekylibre.android.ekyapicom.sdk.api.authentication.token.manager

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ekylibre.android.ekyapicom.sdk.ApiComError
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.api.APICommunicationModule
import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.ekylibre.android.ekyapicom.sdk.api.ApiInterface
import com.ekylibre.android.ekyapicom.sdk.api.authentication.token.response.SimpleTokenResponse
import com.ekylibre.android.ekyapicom.sdk.api.dataManagers.WorkerProfileManager
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import com.ekylibre.android.ekyapicom.sdk.database.models.AccountDB
import retrofit2.Call
import retrofit2.Response
import kotlin.concurrent.thread

class SimpleTokenManager(
    override var apiUrl: String,
    override var context: Context,
    var ekyDbSDK: EkyDbSDK
): TokenManager, APICommunicationModule, ViewModel() {

    override var username = ""
    override var password = ""

    private val tokenLiveData = MutableLiveData<String>()

    override val errorSharedPrefsKey: String
        get() = "authentication_error"

    override fun generateAccessToken() {
        val apiInterface: ApiInterface = ApiInterface.getAuthClient(apiUrl)!!.create(ApiInterface::class.java)
        val tokenResponse = apiInterface.getToken(username, password)

        enqueueResponse(apiUrl, tokenResponse as Call<APIResponse>, context)
    }

    override fun refreshAccessToken(refreshToken: String?) {
        generateAccessToken()
    }

    override fun onResponseCallback(r: Response<APIResponse>) {
        val response = r as Response<SimpleTokenResponse>
        if (response.isSuccessful && response.body() != null) {
            saveAuthSuccess(response)

            tokenLiveData.value = response.body()!!.token
        } else
            saveError(
                ApiComError.getApiComErrorFromMessage(response.message()).get(context),
                apiUrl,
                context
            )
    }

    private fun saveAuthSuccess(response: Response<SimpleTokenResponse>) {
        val accountAsList = arrayListOf(AccountDB(username, response.body()!!.token, apiUrl))
        val storedAccountsIds = ekyDbSDK.storeAccounts(accountAsList)
        val valuesToStore = mapOf("access_token" to response.body()!!.token, "api_url" to apiUrl, "username" to username, "account_id" to storedAccountsIds[0].toString(), username+"_inter_to_sync" to "request", username+"_theme" to false)
        Utils.saveInSharedPreferences(valuesToStore, context)
        thread { WorkerProfileManager(context, ekyDbSDK).pull() }
    }
}