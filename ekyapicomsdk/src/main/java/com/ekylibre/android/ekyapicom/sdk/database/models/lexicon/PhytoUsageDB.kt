package com.ekylibre.android.ekyapicom.sdk.database.models.lexicon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PhytoUsageDB(
    @PrimaryKey @ColumnInfo(name = "usage_eky_id") val ekyId: String,
    @ColumnInfo(name = "usage_product_id") val productId: Long,
    @ColumnInfo(name = "usage_ephy_phrase") val ephyUsagePhrase: String,
    @ColumnInfo(name = "usage_species") val species: String,
    @ColumnInfo(name = "usage_target_name") val targetName: String,
    @ColumnInfo(name = "usage_description") val description: String,
    @ColumnInfo(name = "usage_treatment") val treatment: String,
    @ColumnInfo(name = "usage_dose_quantity") val doseQuantity: String,
    @ColumnInfo(name = "usage_dose_unit") val doseUnit: String,
    @ColumnInfo(name = "usage_dose_unit_name") val doseUnitName: String,
    @ColumnInfo(name = "usage_dose_unit_factor") val doseUnitFactor: Double,
    @ColumnInfo(name = "usage_pre_harvest_delay") val preHarvestDelay: Long,
    @ColumnInfo(name = "usage_pre_harvest_delay_bbch") val preHarvestDelayBBCH: Long,
    @ColumnInfo(name = "usage_applications_count") val applicationsCount: Long,
    @ColumnInfo(name = "usage_applications_frequency") val applicationsFrequency: Long,
    @ColumnInfo(name = "usage_development_stage_min") val developmentStageMin: Long,
    @ColumnInfo(name = "usage_development_stage_max") val developmentStageMax: Long,
    @ColumnInfo(name = "usage_conditions") val conditions: String,
    @ColumnInfo(name = "usage_untreated_buffer_aquatic") val untreatedBufferAquatic: Long,
    @ColumnInfo(name = "usage_untreated_buffer_arthropod") val untreatedBufferArthropod: Long,
    @ColumnInfo(name = "usage_untreated_buffer_plants") val untreatedBufferPlants: Long,
    @ColumnInfo(name = "usage_decision_date") val decisionDate: String,
    @ColumnInfo(name = "usage_state") val state: String,
)