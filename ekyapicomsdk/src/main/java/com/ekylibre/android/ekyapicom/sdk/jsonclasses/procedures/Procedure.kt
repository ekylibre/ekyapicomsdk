package com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures

import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureDB
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class Procedure(var name: String, var categories: String, var position: String, var parameters: ProcedureParameterWithGroup?) {
    fun adaptForDb(): ProcedureDB {
        return ProcedureDB(name, categories, position)
    }

    companion object {
//        fun createFromProcedureDB(bd: BuildingDivisionDB): Procedure {
//            return BuildingDivision(bd.bd_id, bd.name, NetSurfaceArea(bd.net_surface_area), Date(bd.born_at), bd.variety)
//        }
    }
}