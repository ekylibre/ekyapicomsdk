package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import android.os.Parcelable
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
class NetSurfaceArea(): Parcelable {

    var value: String = ""
    var unit: String = ""

    constructor(s: String): this() {
        val tmp = s.split(" ")
        this.value = tmp[0]
        this.unit = tmp[1]
    }

    constructor(value: String, unit: String): this() {
        this.value = value
        this.unit = unit
    }

    fun prepareForDB(): String {
        val tmp = this.value.split("/")
        this.value = if (tmp.size > 1) (tmp[0].toDouble() / tmp[1].toDouble()).toString()
        else tmp[0]
        return "$value $unit"
    }
}