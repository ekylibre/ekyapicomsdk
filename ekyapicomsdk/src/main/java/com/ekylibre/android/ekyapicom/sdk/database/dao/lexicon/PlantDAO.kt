package com.ekylibre.android.ekyapicom.sdk.database.dao.lexicon

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.MatterDB
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.PlantDB

@Dao
interface PlantDAO {
    @Query("SELECT * FROM plantdb")
    fun getAll(): List<PlantDB>

    @Query("SELECT * FROM plantdb WHERE plant_variety LIKE :variety")
    fun getByVariety(variety: String): List<PlantDB>

    @Query("SELECT * FROM plantdb WHERE plant_id IN (:plantIds)")
    fun loadAllByIds(plantIds: LongArray): List<PlantDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg plantDB: PlantDB)

    @Delete
    fun delete(plantDb: PlantDB)
}