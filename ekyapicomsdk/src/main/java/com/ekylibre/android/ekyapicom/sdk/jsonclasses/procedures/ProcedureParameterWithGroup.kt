package com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures

import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureParameterDB
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ProcedureParameterWithGroup(val group: MutableList<GenericProcedureParameter>?,
                                  val target: MutableList<GenericProductParameter>?,
                                  val output: MutableList<GenericProductParameter>?,
                                  val input: MutableList<GenericProductParameter>?,
                                  val doer: MutableList<GenericProductParameter>?,
                                  val tool: MutableList<GenericProductParameter>?) {

    companion object {
        fun createFromProcedureParametersDB(params: List<ProcedureParameterDB>, products: HashMap<Long, ArrayList<GenericProductParameter>>): ProcedureParameterWithGroup {
            val typesMap: HashMap<String, MutableList<GenericProductParameter>> = hashMapOf("target" to ArrayList(),
                                                                                            "output" to ArrayList(),
                                                                                            "input" to ArrayList(),
                                                                                            "doer" to ArrayList(),
                                                                                            "tool" to ArrayList())
            val groupTypesMap: HashMap<String, MutableList<GenericProductParameter>> = hashMapOf("target" to ArrayList(),
                    "output" to ArrayList(),
                    "input" to ArrayList(),
                    "doer" to ArrayList(),
                    "tool" to ArrayList())

            var groupName = ""

            params.map { param ->
                if (param.isGroup != "") {
                    groupName = param.isGroup
                    products[param.id]?.map {
                        groupTypesMap[param.type]?.add(it)
                    }
                } else {
                    products[param.id]?.map {
                        typesMap[param.type]?.add(it)
                    }
                }
            }
            val group: MutableList<GenericProcedureParameter>? = if (groupName != "")
                mutableListOf(GenericProcedureParameter(groupName, groupTypesMap["target"], groupTypesMap["output"], groupTypesMap["input"], groupTypesMap["doer"], groupTypesMap["tool"]))
            else null
            return ProcedureParameterWithGroup(group, typesMap["target"], typesMap["output"], typesMap["input"], typesMap["doer"], typesMap["tool"])
        }
    }
}