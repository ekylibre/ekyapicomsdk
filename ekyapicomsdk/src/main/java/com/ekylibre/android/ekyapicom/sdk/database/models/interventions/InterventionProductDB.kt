package com.ekylibre.android.ekyapicom.sdk.database.models.interventions

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey

@Entity//(foreignKeys = [ForeignKey(entity = ParameterDB::class, parentColumns = ["p_id"], childColumns = ["f_p_id"], onDelete = CASCADE)])
class InterventionProductDB (
    @PrimaryKey @ColumnInfo(name = "param_id") val param_id: Long,
    @ColumnInfo(name = "prod_name") val name: String,
    @ColumnInfo(name = "eky_prod_id") val ekyProdId: Long
) {
}