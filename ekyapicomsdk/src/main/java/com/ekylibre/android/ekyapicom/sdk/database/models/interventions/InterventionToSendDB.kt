package com.ekylibre.android.ekyapicom.sdk.database.models.interventions

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class InterventionToSendDB(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "its_id") val id: Long,
        @ColumnInfo(name = "its_account_id") var accountId: Long,
        @ColumnInfo(name = "its_json_body") val jsonBody: String
) {
    constructor(jsonBody: String, accountId: Long) : this(0, accountId, jsonBody)
}