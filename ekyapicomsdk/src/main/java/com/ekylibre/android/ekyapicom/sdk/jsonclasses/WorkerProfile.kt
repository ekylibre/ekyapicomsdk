package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class WorkerProfile(var worker_id: Long) {
}