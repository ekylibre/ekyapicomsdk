package com.ekylibre.android.ekyapicom.sdk.database.dao.interventions

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionProductDB

@Dao
interface InterventionProductDAO {
    @Query("SELECT * FROM interventionproductdb")
    fun getAll(): List<InterventionProductDB>

    @Query("SELECT * FROM interventionproductdb WHERE eky_prod_id IN (:productIds)")
    fun loadAllByIds(productIds: IntArray): List<InterventionProductDB>

    @Query("SELECT * FROM interventionproductdb WHERE param_id IN (:paramIds)")
    fun loadAllByParamIds(paramIds: LongArray): List<InterventionProductDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg interventionProductDB: InterventionProductDB)

    @Delete
    fun delete(interventionProductDB: InterventionProductDB)

    @Query("DELETE FROM interventionproductdb")
    fun deleteAll()
}