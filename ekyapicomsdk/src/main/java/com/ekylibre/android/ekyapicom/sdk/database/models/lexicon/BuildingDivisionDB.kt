package com.ekylibre.android.ekyapicom.sdk.database.models.lexicon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class BuildingDivisionDB (
    @PrimaryKey val bd_id: Long,
    @ColumnInfo(name = "bd_name") val name: String,
    @ColumnInfo(name = "bd_net_surface_area") val net_surface_area: String,
    @ColumnInfo(name = "bd_born_at") val born_at: Long,
    @ColumnInfo(name = "bd_variety") val variety: String
)