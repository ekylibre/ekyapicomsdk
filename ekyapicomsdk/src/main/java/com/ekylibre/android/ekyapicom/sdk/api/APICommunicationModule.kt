package com.ekylibre.android.ekyapicom.sdk.api

import android.content.Context
import com.ekylibre.android.ekyapicom.sdk.ApiComError
import com.ekylibre.android.ekyapicom.sdk.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

interface APICommunicationModule {
    val errorSharedPrefsKey: String
    fun onResponseCallback(r: Response<APIResponse>)

    fun enqueueResponse(apiUrl: String, response: Call<APIResponse>, context: Context) {
        response.enqueue(object : Callback<APIResponse> {
            override fun onFailure(call: Call<APIResponse>?, t: Throwable?) {
                t?.let { onErrorCallback(it, apiUrl, context) }
            }

            override fun onResponse(call: Call<APIResponse>?, response: Response<APIResponse>?) {
                response?.let { onResponseCallback(it) }
            }
        })
    }

    fun onErrorCallback(error: Throwable, apiUrl: String, c: Context) {
        error.message?.let { saveError(ApiComError.getApiComErrorFromMessage(it).get(c), apiUrl, c) }
    }

    fun saveError(error: String, apiUrl: String, context: Context) {
        val valuesToStore = mapOf(errorSharedPrefsKey to ("$error $apiUrl"))
        Utils.saveInSharedPreferences(valuesToStore, context)
    }
}