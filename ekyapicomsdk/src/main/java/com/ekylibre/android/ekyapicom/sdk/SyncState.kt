package com.ekylibre.android.ekyapicom.sdk

enum class SyncState {
    REMOTE,
    LOCAL,
    DELETED
}