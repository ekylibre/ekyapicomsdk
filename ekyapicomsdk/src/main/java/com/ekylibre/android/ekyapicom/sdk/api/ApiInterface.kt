package com.ekylibre.android.ekyapicom.sdk.api

import android.content.Context
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.api.authentication.token.response.OAuthTokenResponse
import com.ekylibre.android.ekyapicom.sdk.api.authentication.token.response.SimpleTokenResponse
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.*
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionResponse
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionToSend
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.PaginatedPhytoResponse
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.PaginatedUsageResponse
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.*
import java.util.concurrent.TimeUnit


interface ApiInterface {

    @POST("/oauth/token")
    @FormUrlEncoded
    fun getOAuthToken(
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("grant_type") grantType: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("scope") scope: String
    ): Call<OAuthTokenResponse>

    @POST("/oauth/token")
    @FormUrlEncoded
    fun getOAuthTokenFromRefreshToken(
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("grant_type") grantType: String,
        @Field("refresh_token") refreshToken: String
    ): Call<OAuthTokenResponse>

    @POST("/api/v2/tokens")
    @FormUrlEncoded
    fun getToken(
        @Field("email") username: String,
        @Field("password") password: String
    ): Call<SimpleTokenResponse>

    @GET("/api/v2/interventions?nature=record")
    fun getRecordInterventions(@Query("user_email") userEmail: String): Call<List<InterventionResponse>>

    @GET("/api/v2/interventions?nature=request")
    fun getRequestInterventions(@Query("user_email") userEmail: String): Call<List<InterventionResponse>>

    @GET("/api/v2/interventions")
    fun getAllInterventions(@Query("user_email") userEmail: String): Call<List<InterventionResponse>>

    @GET("/api/v2/products?product_type=Plant")
    fun getPlantsAsync(@Query("modified_since") modifiedSince: String?): Call<List<Plant>>

    @GET("/api/v2/profile")
    fun getWorkerProfileAsync(): Call<WorkerProfile>

    @GET("/api/v2/variants")
    fun getVariantsAsync(@Query("modified_since") modifiedSince: String?): Call<List<Variant>>

    @GET("/api/v2/products?product_type=workers")
    fun getWorkersAsync(@Query("modified_since") modifiedSince: String?): Call<List<Worker>>

    @GET("/api/v2/products?product_type=equipments")
    fun getEquipmentsAsync(@Query("modified_since") modifiedSince: String?): Call<List<Equipment>>

    @GET("/api/v2/products?product_type=land_parcels")
    fun getLandParcelsAsync(@Query("modified_since") modifiedSince: String?): Call<List<LandParcel>>

    @GET("/api/v2/products?product_type=building_divisions")
    fun getBuildingDivisionsAsync(@Query("modified_since") modifiedSince: String?): Call<List<BuildingDivision>>

    @GET("/api/v2/products?product_type=animal")
    fun getAnimalsAsync(@Query("modified_since") modifiedSince: String?): Call<List<Animal>>

    @GET("/api/v2/products?product_type=matters")
    fun getMattersAsync(@Query("modified_since") modifiedSince: String?): Call<List<Matter>>

    @GET("/api/v2/lexicon/registered_phytosanitary_products?user_product=true")
    fun getPhytosAsync(@Query("modified_since") modifiedSince: String?): Call<PaginatedPhytoResponse>

    @GET("/api/v2/lexicon/registered_phytosanitary_usages?user_product=true")
    fun getPhytoUsagesAsync(@Query("modified_since") modifiedSince: String?): Call<PaginatedUsageResponse>

    // Send intervention
    @POST("/api/v2/interventions")
    fun pushIntervention(@Body body: InterventionToSend): Call<ResponseInterventionId>

    // {"started_at":"2021-05-03T11:39:52+0000","stopped_at":"2021-05-03T12:39:52+0000"}],"doers_attributes":[{"product_id":293,"reference_name":"driver"}],"inputs_attributes":[{"product_id":312,"reference_name":"seeds","quantity_value":12,"quantity_handler":"grains_area_density_in_thousand_per_hectare"}],"group_parameters_attributes":[{"reference_name":"zone","targets_attributes":[{"product_id":1279,"reference_name":"land_parcel"}],"outputs_attributes":[{"variant_id":162,"reference_name":"plant","variety":"","batch_number":""}]}]}


    companion object {

        // Return the basic Retrofit client, as no auth token is stored. Will be used to process to an authentication.
        fun getAuthClient(api_url: String): Retrofit? {
            return getClient(api_url, null)
        }

        // Retrieve all stored data and use those to generate the Retrofit client.
        fun get(context: Context): ApiInterface? {
            val apiUrl = Utils.readStringInSharedPreferences("api_url", context)
            val token = Utils.readStringInSharedPreferences("access_token", context)
            val username = Utils.readStringInSharedPreferences("username", context)
            return getDataCallsClient(apiUrl, username, token)!!.create(ApiInterface::class.java)
        }

        // Return a Retrofit client with the token set in the header. It will be used to retrieve and send data (plants, interventions...)
        private fun getDataCallsClient(api_url: String, username: String, token: String): Retrofit? {
            val interceptor = Interceptor { chain ->
                chain.proceed(
                    chain.request().newBuilder()
                        .addHeader("Authorization", "simple-token $username $token")
                        .addHeader("Content-Type", "application/json; charset=iso-8859-1")
                        .build()
                )
            }
            return getClient(api_url, interceptor)
        }

        // Build the complete Retrofit client, with Moshi JSON parser and HttpLoggingInterceptor to display clean logs.
        private fun getClient(api_url: String, interceptor: Interceptor?): Retrofit? {
            if (api_url == "") return null

            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient.Builder()
            client.apply {
                addInterceptor { chain ->
                    var request = chain.request()
                    request = request.newBuilder().build()
                    val response = chain.proceed(request)
                    response
                }
                addInterceptor(loggingInterceptor)
                readTimeout(2, TimeUnit.MINUTES)
            }

            if (interceptor != null)
                client.addInterceptor(interceptor)

            val moshi = Moshi.Builder()
                .add<Date>(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
                .build()

            return Retrofit.Builder()
                .baseUrl(api_url)
                .client(client.build())
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .build()
        }
    }
}