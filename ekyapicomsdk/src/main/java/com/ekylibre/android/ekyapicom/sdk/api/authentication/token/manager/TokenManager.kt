package com.ekylibre.android.ekyapicom.sdk.api.authentication.token.manager

import android.content.Context

interface TokenManager {

    var apiUrl: String
    var username: String
    var password: String
    var context: Context

    fun generateAccessToken()

    fun refreshAccessToken(refreshToken: String?)

    fun setCredentials(u: String, p: String) {
        username = u
        password = p
    }
}