package com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class GroupParamAttributes(var reference_name: String,
                           var targets_attributes: List<ParamAttributes>?,
                           var outputs_attributes: List<ParamAttributes>?,
                           var inputs_attributes: List<ParamAttributes>?,
                           var tools_attributes: List<ParamAttributes>?,
                           var doers_attributes: List<ParamAttributes>?) {

    fun getPreview(): String {
        return if (targets_attributes.isNullOrEmpty())
            if (this.inputs_attributes.isNullOrEmpty())
                if (this.outputs_attributes.isNullOrEmpty())
                    if (this.tools_attributes.isNullOrEmpty())
                        if (this.doers_attributes.isNullOrEmpty())
                            ""
                        else
                            this.tools_attributes!![0].getPreview()
                    else
                        this.tools_attributes!![0].getPreview()
                else
                    this.outputs_attributes!![0].getPreview()
            else
                this.inputs_attributes!![0].getPreview()
        else this.targets_attributes!![0].getPreview()
    }

}
