package com.ekylibre.android.ekyapicom.sdk.api.dataManagers

import android.content.Context
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.api.APICommunicationModule
import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.ekylibre.android.ekyapicom.sdk.api.ApiInterface
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.Equipment
import retrofit2.Call
import retrofit2.Response

class EquipmentManager(var context: Context, var ekyDbSDK: EkyDbSDK): APICommunicationModule {

    override val errorSharedPrefsKey: String
        get() = "sync_error"

    override fun onResponseCallback(r: Response<APIResponse>) {
        if (r.body() != null && r.isSuccessful) {
            val l: List<Equipment> = r.body()!! as List<Equipment>
            ekyDbSDK.storeEquipments(l.map { e -> e.adaptForDb() })

            Utils.saveInSharedPreferences(mapOf("last_equipments_sync" to Utils.getTodaysDate()), context)
        }
    }

    fun pull() {
        val modifiedSince = Utils.readStringInSharedPreferences("last_equipments_sync", context)
        val response = if (modifiedSince == "") ApiInterface.get(context)?.getEquipmentsAsync(null) else ApiInterface.get(context)?.getEquipmentsAsync(modifiedSince)
        val apiUrl = Utils.readStringInSharedPreferences("api_url", context)
        response?.let { enqueueResponse(apiUrl, it as Call<APIResponse>, context) }
    }
}