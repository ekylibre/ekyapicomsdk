package com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions

import com.ekylibre.android.ekyapicom.sdk.Constants
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class Providers(var data: ProvidersData) {

    var vendor = Constants.PROVIDERS_VENDOR
    var name = Constants.PROVIDERS_NAME
    var id = Constants.PROVIDERS_ID
}