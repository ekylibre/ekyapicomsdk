package com.ekylibre.android.ekyapicom.sdk.database.dao.interventions

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionParameterDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionParameterWithProductDB

@Dao
interface InterventionParameterDAO {
    @Query("SELECT * FROM interventionparameterdb")
    fun getAll(): List<InterventionParameterDB>

    @Query("SELECT * FROM interventionparameterdb WHERE p_id IN (:parameterIds)")
    fun loadAllByIds(parameterIds: LongArray): List<InterventionParameterDB>

    @Query("SELECT * FROM interventionparameterdb WHERE f_i_id IN (:interIds)")
    fun loadAllByInterIds(interIds: LongArray): List<InterventionParameterDB>

    @Query("SELECT * FROM interventionparameterdb WHERE f_i_id == (:id)")
    fun loadAllByInterId(id: Long): List<InterventionParameterDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg interventionDBIntervention: InterventionParameterDB)

    @Delete
    fun delete(interventionDBIntervention: InterventionParameterDB)

    @Query("DELETE FROM interventionparameterdb")
    fun deleteAll()
}