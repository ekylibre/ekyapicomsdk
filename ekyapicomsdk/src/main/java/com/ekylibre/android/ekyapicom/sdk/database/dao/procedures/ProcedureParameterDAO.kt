package com.ekylibre.android.ekyapicom.sdk.database.dao.procedures

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureParameterDB

@Dao
interface ProcedureParameterDAO {
    @Query("SELECT * FROM procedureparameterdb")
    fun getAll(): List<ProcedureParameterDB>

    @Query("SELECT * FROM procedureparameterdb WHERE f_proc_id = :procedureId")
    fun getParametersByProcedureId(procedureId: Long): List<ProcedureParameterDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(procedureParameterDB: ProcedureParameterDB): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg procedureParameterDB: ProcedureParameterDB): List<Long>

    @Query("DELETE FROM procedureparameterdb")
    fun deleteAll()
}