package com.ekylibre.android.ekyapicom.sdk.api.authentication

import android.content.Context
import com.ekylibre.android.ekyapicom.sdk.api.authentication.token.manager.TokenManager

interface AuthModuleInterface {

    var apiUrl: String
    var username: String
    var password: String
    var accessToken: String
    var context: Context
    var tokenManager: TokenManager

    fun init()

    fun start()

    fun create()

    fun destroy() {
        val sharedPreferencesEditor = context.getSharedPreferences(context.packageName, 0).edit()
        sharedPreferencesEditor.remove("access_token")
        sharedPreferencesEditor.remove("api_url")
        sharedPreferencesEditor.apply()
    }

    fun setCredentials(u: String, p: String) {
        username = u
        password = p
        tokenManager.setCredentials(u, p)
    }
}