package com.ekylibre.android.ekyapicom.sdk.api.authentication.token.manager

import android.content.Context
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.api.APICommunicationModule
import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.ekylibre.android.ekyapicom.sdk.api.ApiInterface
import com.ekylibre.android.ekyapicom.sdk.api.authentication.token.response.OAuthTokenResponse
import retrofit2.Call
import retrofit2.Response

class OAuthTokenManager(
    override var apiUrl: String,
    var clientId: String,
    var clientSecret: String,
    override var context: Context
) : TokenManager,
    APICommunicationModule {

    override var username = ""
    override var password = ""
    private var refreshToken = ""

    override val errorSharedPrefsKey: String
        get() = "authentication_error"

    override fun generateAccessToken() {
        val apiInterface: ApiInterface = ApiInterface.getAuthClient(apiUrl)!!.create(ApiInterface::class.java)
        val tokenResponse = apiInterface.getOAuthToken(
            clientId, clientSecret, "password", username, password,
            "public read:profile read:lexicon read:plots read:crops read:interventions write:interventions read:equipment write:equipment read:articles write:articles read:person write:person"
        )
        enqueueResponse(apiUrl, tokenResponse as Call<APIResponse>, context)
    }

    override fun refreshAccessToken(refreshToken: String?) {
        val apiInterface: ApiInterface = ApiInterface.getAuthClient(apiUrl)!!.create(ApiInterface::class.java)

        val tokenResponse = apiInterface.getOAuthTokenFromRefreshToken(
            clientId, clientSecret, "refresh_token", refreshToken!!
        )

        enqueueResponse(apiUrl, tokenResponse as Call<APIResponse>, context)
    }

    override fun onResponseCallback(r: Response<APIResponse>) {
        val response: Response<OAuthTokenResponse> = r as Response<OAuthTokenResponse>
        if (response.isSuccessful && response.body() != null) {
            val valuesToStore = mapOf("access_token" to response.body()!!.access_token, "refresh_token" to response.body()!!.refresh_token, "api_url" to apiUrl, "username" to username)
            Utils.saveInSharedPreferences(valuesToStore, context)
        }
    }
}
