package com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos

import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.PhytoDB
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.NetSurfaceArea
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class Phyto(var id: Long, var reference_name: String, var name: String, var other_names: MutableList<String>?, var natures: MutableList<String>, var active_compounds: MutableList<String>,
            var france_maaid: String, var mix_category_codes: MutableList<Long>, var in_field_reentry_delay: Long, var state: String, var started_on: String?, var stopped_on: String?, var allowed_mentions: PhytoMentions?, var restricted_mentions: PhytoMentions?,
            var operator_protection_mentions: String?, var firm_name: String, var product_type: String, var record_checksum: Long):
    InterItem {

    @Transient
    private var quantity = 0.0
    @Transient
    private var itemUnit = ""

    fun adaptForDb(): PhytoDB {
        return PhytoDB(id, reference_name, name, other_names?.joinToString(",") ?: "", natures.joinToString(), active_compounds.joinToString(), france_maaid, mix_category_codes.joinToString(), in_field_reentry_delay, state, started_on ?: "", stopped_on ?: "",
            "", "", operator_protection_mentions ?: "", firm_name, product_type, record_checksum)
    }

    companion object {
        fun createFromPhytoDB(p: PhytoDB): Phyto {
            return Phyto(p.phyto_id, p.referenceName, p.name, p.otherNames.split(",").toMutableList(), p.natures.split(", ").toMutableList(), p.activeCompounds.split(", ").toMutableList(), p.maaid, p.mixCategoryCodes.split(", ").toMutableList().map { c -> c.toLong() }.toMutableList(),
                p.inFieldReentryDelay, p.state, p.startedOn, p.stoppedOn, null, null, p.operatorProtectionMentions, p.firmName, p.productType, p.recordChecksum)
        }
    }

    override fun getProductId(): Long {
        return this.id
    }

    override fun getProductName(): String {
        return this.reference_name
    }

    override fun setQuantity(q: Double?, unit: String?) {
        q?.let { this.quantity = q }
        unit?.let { this.itemUnit = unit }
    }
    override fun getQuantity(): Double { return this.quantity }
    override fun getUnit(): String { return this.itemUnit }
    override fun getHours(): Double? { return null }
    override fun getItemVariety(): String? { return null }
    override fun getItemAbilities(): List<String> { return emptyList() }
    override fun getStartDate(): Date? { return Utils.createDate(this.started_on) }
    override fun getEndDate(): Date? { return Utils.createDate(this.stopped_on) }
    override fun getDerivative(): String? { return null }
    override fun getSurfaceArea(): NetSurfaceArea? { return null }

    override fun getSvgShape(): String? { return null }
}