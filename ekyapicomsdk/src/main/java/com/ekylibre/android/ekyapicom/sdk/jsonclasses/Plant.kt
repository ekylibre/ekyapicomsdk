package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.PlantDB
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class Plant(var id: Long,  var name: String, var net_surface_area: NetSurfaceArea, var variety: String, var activity_id: Long?, var activity_name: String?, var production_started_on: String?, var production_stopped_on: String?, var shape_svg: String?): InterItem {

    fun adaptForDb(): PlantDB {
        return PlantDB(id, name, net_surface_area.prepareForDB(), variety, activity_id ?: -1,
            activity_name ?: "", production_started_on ?: "", production_stopped_on ?: "", shape_svg ?: "")
    }

    companion object {
        fun createFromPlantDB(p: PlantDB): Plant {
            return Plant(p.plant_id, p.name, NetSurfaceArea(p.netSurfaceArea), p.variety, p.activityId, p.activityName, p.productionStartedOn, p.productionStoppedOn, p.shapeSvg)
        }
    }

    override fun getProductId(): Long {
        return this.id
    }

    override fun getProductName(): String {
        return this.name
    }

    override fun setQuantity(q: Double?, unit: String?) {}
    override fun getQuantity(): Double? { return null }
    override fun getUnit(): String? { return null }
    override fun getHours(): Double? { return null }
    override fun getItemVariety(): String { return this.variety }
    override fun getItemAbilities(): List<String> { return emptyList() }
    override fun getStartDate(): Date? { return Utils.createDate(this.production_started_on) }
    override fun getEndDate(): Date? { return Utils.createDate(this.production_stopped_on) }
    override fun getDerivative(): String? { return null }
    override fun getSurfaceArea(): NetSurfaceArea? { return this.net_surface_area }

    override fun getSvgShape(): String? {
        return this.shape_svg
    }
}