package com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
open class Costing(var total: Double, var inputs: Double, var doers: Double, var tools: Double, var receptions: Double) {
}