package com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures

import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureHandlerDB
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureProductDB
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class GenericProductParameter(val name: String?, val filter: String?, val handler: MutableList<HandlerParameter>?, val cardinality: String?) {
    fun adaptForDb(procedureParameterId: Long): ProcedureProductDB {
        return ProcedureProductDB(name ?: "", filter ?: "", cardinality ?: "", procedureParameterId)
    }

    companion object {
        fun createFromDB(p: ProcedureProductDB, h: List<ProcedureHandlerDB>): GenericProductParameter {
            val handlers = ArrayList<HandlerParameter>()
            h.map { handlers.add(HandlerParameter.createFromDB(it)) }
            return GenericProductParameter(p.name, p.filter, handlers, p.cardinality)
        }
    }
}