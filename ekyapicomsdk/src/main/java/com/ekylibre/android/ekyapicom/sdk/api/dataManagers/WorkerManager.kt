package com.ekylibre.android.ekyapicom.sdk.api.dataManagers

import android.content.Context
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.api.APICommunicationModule
import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.ekylibre.android.ekyapicom.sdk.api.ApiInterface
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.Worker
import retrofit2.Call
import retrofit2.Response

class WorkerManager(var context: Context, var ekyDbSDK: EkyDbSDK): APICommunicationModule {

    override val errorSharedPrefsKey: String
        get() = "sync_error"

    override fun onResponseCallback(r: Response<APIResponse>) {
        if (r.body() != null && r.isSuccessful) {
            val workersToStore: List<Worker> = r.body()!! as List<Worker>
            ekyDbSDK.storeWorker(workersToStore.map { w -> w.adaptForDb() })

            Utils.saveInSharedPreferences(mapOf("last_workers_sync" to Utils.getTodaysDate()), context)
        }
    }

    fun pull() {
        val modifiedSince = Utils.readStringInSharedPreferences("last_workers_sync", context)
        val response = if (modifiedSince == "") ApiInterface.get(context)?.getWorkersAsync(null) else ApiInterface.get(context)?.getWorkersAsync(modifiedSince)
        val apiUrl = Utils.readStringInSharedPreferences("api_url", context)
        response?.let { enqueueResponse(apiUrl, response as Call<APIResponse>, context) }
    }
}