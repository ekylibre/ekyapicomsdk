package com.ekylibre.android.ekyapicom.sdk.database.models.procedures

enum class ProcedureParameterTypes(val typeName: String) {
    DOER("doer"),
    TARGET("target"),
    TOOL("tool"),
    INPUT("input"),
    OUTPUT("output")
}