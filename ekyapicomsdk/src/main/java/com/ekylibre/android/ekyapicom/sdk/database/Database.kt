package com.ekylibre.android.ekyapicom.sdk.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ekylibre.android.ekyapicom.sdk.database.dao.*
import com.ekylibre.android.ekyapicom.sdk.database.dao.interventions.*
import com.ekylibre.android.ekyapicom.sdk.database.dao.lexicon.*
import com.ekylibre.android.ekyapicom.sdk.database.dao.procedures.ProcedureDAO
import com.ekylibre.android.ekyapicom.sdk.database.dao.procedures.ProcedureHandlerDAO
import com.ekylibre.android.ekyapicom.sdk.database.dao.procedures.ProcedureParameterDAO
import com.ekylibre.android.ekyapicom.sdk.database.dao.procedures.ProcedureProductDAO
import com.ekylibre.android.ekyapicom.sdk.database.models.*
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionParameterDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionProductDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionToSendDB
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.*
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureProductDB
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureDB
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureHandlerDB
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureParameterDB

@Database(entities = [InterventionDB::class,
    InterventionParameterDB::class, InterventionProductDB::class,
    PlantDB::class, WorkerDB::class,
    EquipmentDB::class, LandParcelDB::class,
    BuildingDivisionDB::class, MatterDB::class, VariantDB::class,
    AccountDB::class, ProcedureDB::class, ProcedureParameterDB::class,
    ProcedureProductDB::class, ProcedureHandlerDB::class,
    InterventionToSendDB::class, PhytoDB::class, PhytoUsageDB::class, AnimalDB::class], version = 1)

abstract class Database: RoomDatabase() {
    abstract fun interventionDAO(): InterventionDAO
    abstract fun interventionParameterDAO(): InterventionParameterDAO
    abstract fun productDAO(): InterventionProductDAO
    abstract fun plantDAO(): PlantDAO
    abstract fun workerDAO(): WorkerDAO
    abstract fun equipmentDAO(): EquipmentDAO
    abstract fun landParcelDAO(): LandParcelDAO
    abstract fun buildingDivisionDAO(): BuildingDivisionDAO
    abstract fun matterDAO(): MatterDAO
    abstract fun variantDAO(): VariantDAO
    abstract fun accountDAO(): AccountDAO
    abstract fun procedureDAO(): ProcedureDAO
    abstract fun procedureParameterDAO(): ProcedureParameterDAO
    abstract fun procedureProductParameterDAO(): ProcedureProductDAO
    abstract fun procedureHandlerDAO(): ProcedureHandlerDAO
    abstract fun interventionToSendDAO(): InterventionToSendDAO
    abstract fun phytoDAO(): PhytoDAO
    abstract fun phytoUsageDAO(): PhytoUsageDAO
    abstract fun animalDAO(): AnimalDAO
}