package com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions

import com.squareup.moshi.JsonClass
import java.text.SimpleDateFormat
import java.util.*

@JsonClass(generateAdapter = true)
class WorkingPeriodsAttributes(var started_at: String, var stopped_at: String) {

    companion object {
        fun fromCalendar(startCalendar: Calendar?, endCalendar: Calendar?): WorkingPeriodsAttributes {
            return WorkingPeriodsAttributes(formatCalendar(startCalendar), formatCalendar(endCalendar))
        }

        private fun formatCalendar(c: Calendar?): String {
            if (c == null) return ""

            val offsetFromUtc = TimeZone.getDefault().getOffset(Date().getTime()) / 3600000
            val fmt = SimpleDateFormat("yyy-MM-dd HH:mm+" + offsetFromUtc.toString().padStart(4, '0'))
            return fmt.format(c.time).replace(' ', 'T')
        }
    }
}