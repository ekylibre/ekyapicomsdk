package com.ekylibre.android.ekyapicom.sdk

import android.content.Context

class Constants {

    companion object {
        const val PROVIDERS_VENDOR = "ekylibre"
        const val PROVIDERS_NAME = "zero"
        const val PROVIDERS_ID = 0

        // Output attributes keys
        const val OUTPUT_ATTR_NET_MASS = "net_mass"
        const val OUTPUT_ATTR_SEX = "sex"
        const val OUTPUT_ATTR_MAMMALIA_BIRTH_CDT = "mammalia_birth_condition"
        const val OUTPUT_ATTR_HEALTHY = "healthy"
        const val OUTPUT_ATTR_HOUR_COUNTER = "hour_counter"

        // Sexes
        val MALE = "male"
        val FEMALE = "female"
        val HERMAPHRODITE = "hermaphrodite"
        val UNDEFINED = "undefined"

        // Birth conditions
        val WITHOUT_HELP = "without_help"
        val CAESAREAN = "caesarean"
        val FEW_HELP = "few_help"
        val GREAT_HELP = "great_help"
        val NEWBORN_CUTTING = "newborn_cutting"

        // Units
        val KILOGRAM = "kilogram"
        val GRAM = "gram"
        val QUINTAL = "quintal"
        val MILLIGRAM = "milligram"
        val MICROGRAM = "microgram"
        val TON = "ton"
        val KILOGRAM_PER_HECTARE = "kilogram_per_hectare"
        val UNITY = "unity"
        val UNITY_PER_SQUARE_METER = "unity_per_square_meter"
        val THOUSAND_PER_HECTARE = "thousand_per_hectare"
        val THOUSAND = "thousand"
        val POPULATION = ""
        val LITER_PER_HECTARE = "liter_per_hectare"
        val LITER_PER_SQUARE_METER = "liter_per_square_meter"
        val HECTARE = "hectare"
        val SQUARE_METER = "square_meter"
        val CUBIC_METER = "cubic_meter"
        val QUINTAL_PER_HECTARE = "quintal"

        val genderMap = mapOf(
            MALE to R.string.male,
            FEMALE to R.string.female,
            HERMAPHRODITE to R.string.hermaphrodite,
            UNDEFINED to R.string.undefined
        )

        val birthConditionMap = mapOf(
            WITHOUT_HELP to R.string.without_help,
            CAESAREAN to R.string.caesarean,
            FEW_HELP to R.string.few_help,
            GREAT_HELP to R.string.great_help,
            NEWBORN_CUTTING to R.string.new_born_cutting
        )

        val netMassUnits = mapOf(
            TON to R.string.ton,
            QUINTAL to R.string.quintal,
            KILOGRAM to R.string.kilogram,
            GRAM to R.string.gram,
            MILLIGRAM to R.string.milligram,
            MICROGRAM to R.string.microgram
        )

        private val unitsMap = mapOf(TON to R.string.ton,
            CUBIC_METER to R.string.cubic_meter,
            QUINTAL to R.string.quintal,
            QUINTAL_PER_HECTARE to R.string.quintal_per_hectare,
            KILOGRAM to R.string.kilogram,
            GRAM to R.string.gram,
            MILLIGRAM to R.string.milligram,
            MICROGRAM to R.string.microgram,
            KILOGRAM_PER_HECTARE to R.string.kilogram_per_hectare,
            UNITY to R.string.unity,
            UNITY_PER_SQUARE_METER to R.string.unity_per_square_meter,
            THOUSAND_PER_HECTARE to R.string.thousand_per_hectare,
            THOUSAND to R.string.thousand,
            POPULATION to R.string.population,
            LITER_PER_HECTARE to R.string.liter_per_hectare,
            LITER_PER_SQUARE_METER to R.string.liter_per_square_meter
        )

        private val areaUnitsMap = mapOf(HECTARE to R.string.hectare, SQUARE_METER to R.string.square_meter)

        fun getUnitString(u: String, c: Context): String {
            unitsMap[u]?.let { return c.getString(it) }
            areaUnitsMap[u]?.let { return c.getString(it) }
            return u
        }

        fun getUnitKeyFromString(key: String?, c:Context): String? {
            return getKeyFromString(key, this.unitsMap, c)
        }

        fun getGenderKeyFromString(key: String?, c:Context): String? {
            return getKeyFromString(key, this.genderMap, c)
        }

        fun getBirthCdtKeyFromString(key: String?, c:Context): String? {
            return getKeyFromString(key, this.birthConditionMap, c)
        }

        private fun getKeyFromString(key: String?, map: Map<String, Int>, c: Context): String? {
            map.keys.map {
                if (c.getString(map[it]!!) == key)
                    return it
            }
            return null
        }
    }
}