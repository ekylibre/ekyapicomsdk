package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.VariantDB
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class Variant(var id: Long, var name: String, var number: String, var variety: String, var derivative_of: String?, var nature_abilities: MutableList<String>): InterItem {

    @Transient
    private var quantity = 0.0
    @Transient
    private var itemUnit = ""
    @Transient
    private var newBornName = ""
    @Transient
    private var newBornID = 0L
    @Transient
    private var newBornGender = ""
    @Transient
    private var newBornBirthCondition = ""
    @Transient
    private var isNewBornHealthy = true

    override fun setQuantity(q: Double?, unit: String?) {
        q?.let { this.quantity = q }
        unit?.let { this.itemUnit = unit }
    }

    fun adaptForDb(): VariantDB {
        return VariantDB(id, name, number, variety, derivative_of ?: "", nature_abilities.joinToString())
    }

    companion object {
        fun createFromVariantDB(v: VariantDB): Variant {
            return Variant(v.id, v.name, v.number, v.variety, v.derivativeOf, v.abilities.split(", ").toMutableList())
        }
    }

    fun setNewbornInfos(name: String?, idNumber: Long?, sex: String?, birthCdt: String?, healthy: Boolean?) {
        name?.let { this.newBornName = it }
        idNumber?.let { this.newBornID = it }
        sex?.let { this.newBornGender = it }
        birthCdt?.let { this.newBornBirthCondition = it }
        healthy?.let { this.isNewBornHealthy = it }
    }

    fun isNewBornHealthy(): Boolean { return this.isNewBornHealthy }
    fun getNewBornID(): Long { return this.newBornID }
    fun getNewBornName(): String { return this.newBornName }
    fun getNewBornBirthCondition(): String { return this.newBornBirthCondition }
    fun getNewBornGender(): String { return this.newBornGender }
    override fun getUnit(): String { return if (this.itemUnit.isEmpty()) "population" else this.itemUnit }
    override fun getQuantity(): Double { return this.quantity }
    override fun getHours(): Double? { return null }
    override fun getItemVariety(): String { return this.variety }
    override fun getItemAbilities(): List<String> { return this.nature_abilities }
    override fun getStartDate(): Date? { return null }
    override fun getEndDate(): Date? { return null }
    override fun getDerivative(): String? { return this.derivative_of }
    override fun getSurfaceArea(): NetSurfaceArea? { return null }
    override fun getSvgShape(): String? { return null }
    override fun getProductId(): Long { return this.id }
    override fun getProductName(): String { return this.name }
}