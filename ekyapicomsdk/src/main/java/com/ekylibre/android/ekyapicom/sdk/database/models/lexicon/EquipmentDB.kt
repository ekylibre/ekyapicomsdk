package com.ekylibre.android.ekyapicom.sdk.database.models.lexicon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class EquipmentDB (
    @PrimaryKey val e_id: Long,
    @ColumnInfo(name = "e_name") val name: String,
    @ColumnInfo(name = "e_number") val number: String,
    @ColumnInfo(name = "e_variety") val variety: String,
    @ColumnInfo(name = "e_born_at") val born_at: Long,
    @ColumnInfo(name = "e_abilities") val abilities: String,
    @ColumnInfo(name = "e_work_number") val work_number: String,
    @ColumnInfo(name = "e_has_hour_counter") val has_hour_counter: Boolean
)