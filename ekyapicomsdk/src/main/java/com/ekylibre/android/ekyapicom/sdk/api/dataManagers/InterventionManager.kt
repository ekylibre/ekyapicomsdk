package com.ekylibre.android.ekyapicom.sdk.api.dataManagers

import android.content.Context
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.api.APICommunicationModule
import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.ekylibre.android.ekyapicom.sdk.api.ApiInterface
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionParameterDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionProductDB
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionResponse
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionToSend
import retrofit2.Call
import retrofit2.Response

class InterventionManager(var context: Context, var ekyDbSDK: EkyDbSDK): APICommunicationModule {

    override val errorSharedPrefsKey: String
        get() = "sync_error"

    override fun onResponseCallback(r: Response<APIResponse>) {
        if (r.body() != null && r.isSuccessful) {
            val interventionsToStore: List<InterventionResponse>? = r.body()!! as? List<InterventionResponse>
            if (interventionsToStore != null) {
                storeInterventions(interventionsToStore)
            }
        }
    }

    private fun storeInterventions(interventionsToStore: List<InterventionResponse>) {
        val parametersToStore: List<InterventionParameterDB> =
            interventionsToStore.flatMap { i -> i.parameters.map { p -> p.adaptForDb(i.id) } }
                .distinctBy { it.p_id }
        //TODO: Uncomment those lines when remote intervention got quantities
//        val productsToStore: List<InterventionProductDB> =
//            interventionsToStore.flatMap { i -> i.parameters }
//                .map { p -> p.product.adaptForDb(p.id) }

        ekyDbSDK.storeIntervention(interventionsToStore.map { i ->
            i.adaptForDb(
                Utils.readStringInSharedPreferences(
                    "account_id",
                    context
                ).toLong()
            )
        })
        ekyDbSDK.storeParameters(parametersToStore)
        //TODO: Uncomment those lines when remote intervention got quantities
        //ekyDbSDK.storeProducts(productsToStore)

        Utils.saveInSharedPreferences(
            mapOf(
                "last_interventions_sync" to Utils.getTodaysDate()
            ), context
        )
    }

    fun pullInterventions() {
        val response = when (Utils.readStringInSharedPreferences(Utils.readStringInSharedPreferences("username", context)+"_inter_to_sync", context)) {
            "record" -> ApiInterface.get(context)?.getRecordInterventions(Utils.readStringInSharedPreferences("username", context))
            "both" -> ApiInterface.get(context)?.getAllInterventions(Utils.readStringInSharedPreferences("username", context))
            else -> ApiInterface.get(context)?.getRequestInterventions(Utils.readStringInSharedPreferences("username", context))
        }
        val apiUrl = Utils.readStringInSharedPreferences("api_url", context)
        response?.let { enqueueResponse(apiUrl, response as Call<APIResponse>, context) }
    }

    fun pushIntervention(i: InterventionToSend) {
        val response = ApiInterface.get(context)?.pushIntervention(i)
        val apiUrl = Utils.readStringInSharedPreferences("api_url", context)
        response?.let { enqueueResponse(apiUrl, response as Call<APIResponse>, context) }
    }
}