package com.ekylibre.android.ekyapicom.sdk.api.authentication

import android.content.Context
import android.util.Log
import com.ekylibre.android.ekyapicom.sdk.api.authentication.token.manager.OAuthTokenManager
import com.ekylibre.android.ekyapicom.sdk.api.authentication.token.manager.TokenManager

class OAuthModule(
    override var apiUrl: String,
    clientId : String,
    clientSecret: String,
    override var context: Context
) : AuthModuleInterface {

    override var username = ""
    override var password = ""
    override var accessToken = ""
    override var tokenManager: TokenManager =
        OAuthTokenManager(
            apiUrl,
            clientId,
            clientSecret,
            context
        )

    override fun init() {
        val sharedPreferences = context.getSharedPreferences(context.packageName, 0)
        val accessToken = sharedPreferences.getString("access_token", "")!!
        val refreshToken = sharedPreferences.getString("refresh_token", "")!!

        if (accessToken == "" || refreshToken == "")
            this.create()
        else
            this.start()
    }

    override fun start() {
        Log.e("Step: ", "Refreshing OAuth access token...")
        val sharedPreferences = context.getSharedPreferences(context.packageName, 0)
        val refreshToken = sharedPreferences.getString("refresh_token", "")!!
        tokenManager.refreshAccessToken(refreshToken)
    }

    override fun create() {
        Log.e("Step: ", "Creating OAuth access token...")
        tokenManager.generateAccessToken()
    }

    override fun destroy() {
        super.destroy()

        val sharedPreferencesEditor = context.getSharedPreferences(context.packageName, 0).edit()
        sharedPreferencesEditor.remove("refresh_token")
        sharedPreferencesEditor.apply()
    }
}