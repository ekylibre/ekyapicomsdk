package com.ekylibre.android.ekyapicom.sdk.database.dao

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.AccountDB

@Dao
interface AccountDAO {
    @Query("SELECT * FROM accountdb")
    fun getAll(): List<AccountDB>

    @Query("SELECT * FROM accountdb WHERE a_id IN (:accountIds)")
    fun loadAllByIds(accountIds: IntArray): List<AccountDB>

    @Query("SELECT * FROM accountdb WHERE a_id != (:accountId)")
    fun loadAllButOne(accountId: Int): List<AccountDB>

    @Query("SELECT * FROM accountdb WHERE a_api_url == (:url) AND a_username == (:username)")
    fun loadByNameAndUrl(url: String, username: String): List<AccountDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg accountDB: AccountDB): List<Long>

    @Delete
    fun delete(accountDB: AccountDB)

    @Query("UPDATE accountdb SET a_worker_eky_id = :id WHERE a_id = :accountId")
    fun setAccountWorkerId(id: Long, accountId: Long)
}