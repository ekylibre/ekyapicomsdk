package com.ekylibre.android.ekyapicom.sdk.database.models.interventions

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity//(foreignKeys = [ForeignKey(entity = InterventionDB::class, parentColumns = ["i_id"], childColumns = ["f_i_id"])])
data class InterventionParameterDB(
    @PrimaryKey val p_id: Long,
    @ColumnInfo(name = "p_role") val role: String,
    @ColumnInfo(name = "p_name") val name: String,
    @ColumnInfo(name = "p_label") val label: String,
    @ColumnInfo(name = "p_product_id") val productId: Long,
    @ColumnInfo(name = "f_i_id") val foreignInterId: Long
    ) {
}