package com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class PhytoMentions(var organic_usage: Boolean?, var biocontrol_usage: Boolean?, var bee_usage: Boolean?) {
}