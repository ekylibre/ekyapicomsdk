package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import java.util.*

interface InterItem {
    fun getProductId(): Long
    fun getProductName(): String
    fun setQuantity(q: Double?, unit: String?)
    fun getQuantity(): Double?
    fun getUnit(): String?
    fun getHours(): Double?
    fun getItemVariety(): String?
    fun getItemAbilities(): List<String>
    fun getStartDate(): Date?
    fun getEndDate(): Date?
    fun getDerivative(): String?
    fun getSurfaceArea(): NetSurfaceArea?
    fun getSvgShape(): String?
}