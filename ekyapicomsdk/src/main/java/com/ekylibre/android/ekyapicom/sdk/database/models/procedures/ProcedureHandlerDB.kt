package com.ekylibre.android.ekyapicom.sdk.database.models.procedures

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ProcedureHandlerDB(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "procedure_handler_id") val id: Long,
        @ColumnInfo(name = "procedure_handler_name") val name: String,
        @ColumnInfo(name = "procedure_handler_indicator") val indicator: String,
        @ColumnInfo(name = "procedure_handler_unit") val unit: String,
        @ColumnInfo(name = "procedure_product_parameter_foreign_id") val procedureProductParameterId: Long
) {
    constructor(name: String, indicator: String, unit: String, procedureProductParameterId: Long) : this(0, name, indicator, unit, procedureProductParameterId)
}