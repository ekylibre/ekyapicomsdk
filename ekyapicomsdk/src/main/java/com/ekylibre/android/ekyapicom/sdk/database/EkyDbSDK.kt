package com.ekylibre.android.ekyapicom.sdk.database

import android.content.Context
import android.util.Log
import androidx.room.Room
import com.ekylibre.android.ekyapicom.sdk.database.models.*
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.*
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.*
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.*
import com.ekylibre.android.ekyapicom.sdk.filter.Onthology
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.*
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.Phyto
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.GenericProcedureParameter
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.GenericProductParameter
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.Procedure
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import kotlin.NumberFormatException

class EkyDbSDK(var context: Context) {
    lateinit var db: Database

    fun build() {
        db = Room.databaseBuilder(
            context,
            Database::class.java, "ekylibre_db"
        ).build()
    }

    // ------------ Accounts ------------ //
    fun storeAccounts(accountDbList: List<AccountDB>): List<Long> {
        lateinit var storedAccountsIds: List<Long>
        val process = GlobalScope.async { accountDbList.map { a -> storedAccountsIds = db.accountDAO().insertAll(a) } }
        runBlocking { process.await() }
        Log.e("Storing ${accountDbList.size} accounts", "OK")

        return storedAccountsIds
    }

    fun deleteAccount(accountDb: AccountDB) {
        val process = GlobalScope.async { db.accountDAO().delete(accountDb) }
        runBlocking { process.await() }
        Log.e("Deleting account id ${accountDb.a_id}", "OK")
    }

    fun getStoredAccounts(): List<AccountDB> {
        lateinit var res: List<AccountDB>
        val storedAccounts = GlobalScope.async { res = db.accountDAO().getAll() }
        runBlocking { storedAccounts.await() }

        return res
    }

    fun getStoredAccounts(url: String, username: String): List<AccountDB> {
        lateinit var res: List<AccountDB>
        val storedAccounts = GlobalScope.async { res = db.accountDAO().loadByNameAndUrl(url, username) }
        runBlocking { storedAccounts.await() }

        return res
    }

    fun getStoredAccounts(ids: IntArray): AccountDB {
        lateinit var res: AccountDB
        val storedAccounts = GlobalScope.async { res = db.accountDAO().loadAllByIds(ids)[0] }
        runBlocking { storedAccounts.await() }

        return res
    }

    fun getInactiveAccounts(currentAccountId: Int): List<AccountDB> {
        lateinit var res: List<AccountDB>
        val storedAccounts = GlobalScope.async { res = db.accountDAO().loadAllButOne(currentAccountId) }
        runBlocking { storedAccounts.await() }

        return res
    }

    fun storeAccountWorkerId(id: Long) {
        val process = GlobalScope.async { db.accountDAO().setAccountWorkerId(id, Utils.readStringInSharedPreferences("account_id", context).toLong()) }
        runBlocking { process.await() }
    }

    // ------------ Interventions ------------ //
    fun storeIntervention(interventionDBList: List<InterventionDB>): List<Long> {
        val accountId = Utils.readStringInSharedPreferences("account_id", context)
        interventionDBList.map { it.accountId = accountId.toLong() }
        lateinit var ids: List<Long>
        val process = GlobalScope.async {
            ids = db.interventionDAO().insertAll(*interventionDBList.toTypedArray())
        }
        runBlocking { process.await() }
        Log.v("Storing #${interventionDBList.size} interventions", "OK")
        return ids
    }

    fun storeLocalJSONIntervention(json: String) {
        val accountId = Utils.readStringInSharedPreferences("account_id", context)
        val process = GlobalScope.async {
            try {
                db.interventionToSendDAO().insertAll(InterventionToSendDB(json, accountId.toLong()))
            } catch (e: NumberFormatException) {
                Log.e("StoreLocalJSONInter", e.message ?: "")
            }
        }
        runBlocking { process.await() }
        Log.v("DB: ", "Storing temporary intervention --> OK")
    }

    fun deleteLocalJSONIntervention(json: String, oldId: Long) {
        val accountId = Utils.readStringInSharedPreferences("account_id", context)
        deleteLocalJSONIntervention(InterventionToSendDB(oldId, accountId.toLong(), json))
    }

    fun deleteLocalJSONIntervention(i: InterventionToSendDB) {
        val process = GlobalScope.async {
            db.interventionToSendDAO().delete(i)
        }
        runBlocking { process.await() }
        Log.v("DB: ", "Storing temporary intervention --> OK")
    }

    fun getInterventionsToSend(): List<InterventionToSendDB> {
        lateinit var res: List<InterventionToSendDB>
        val storedInterventions = GlobalScope.async { res = db.interventionToSendDAO().getAll() }
        runBlocking { storedInterventions.await() }

        return res
    }

    fun getStoredRemoteInterventions(): List<InterventionDB> {
        lateinit var res: List<InterventionDB>
        val storedInterventions = GlobalScope.async { res = db.interventionDAO().getAll() }
        runBlocking { storedInterventions.await() }

        return res
    }

    fun getDetailedStoredInterventions(): List<InterventionWithParameterAndProductDB> {
        lateinit var interventions: List<InterventionDB>
        val accountId = Utils.readStringInSharedPreferences("account_id", context)

        val storedInterventionsRetrieval = GlobalScope.async { interventions =
            try {
                db.interventionDAO().loadAllByAccount(accountId.toLong())
            } catch (e: NumberFormatException) {
                ArrayList()
            }
        }
        val parametersGroupedByInterId: Map<Long, List<InterventionParameterWithProductDB>> =
            getStoredParametersWithProducts()?.groupBy { it.parameter.foreignInterId } ?: HashMap()

        runBlocking { storedInterventionsRetrieval.await() }

        return interventions.map { i -> InterventionWithParameterAndProductDB(i, parametersGroupedByInterId.getValue(i.i_id)) }
    }

    fun getDetailedStoredIntervention(id: Long): InterventionWithParameterAndProductDB {
        lateinit var intervention: InterventionDB

        val storedInterventionRetrieval = GlobalScope.async { intervention = db.interventionDAO().get(id) }
        val parametersGroupedByInterId: Map<Long, List<InterventionParameterWithProductDB>> =
            getStoredParametersWithProducts()?.groupBy { it.parameter.foreignInterId } ?: HashMap()

        runBlocking {
            storedInterventionRetrieval.await()
        }

        return InterventionWithParameterAndProductDB(intervention, parametersGroupedByInterId.getValue(intervention.i_id))
    }

    fun destroyInterventions() {
        val process =  GlobalScope.async {
            db.interventionDAO().deleteAll()
            db.interventionParameterDAO().deleteAll()
            db.productDAO().deleteAll()
        }
        runBlocking { process.await() }
        Log.e("Clearing all inter", "OK")
    }
    // ------------ Parameters ------------ //
    fun storeParameters(interventionParameterDbList: List<InterventionParameterDB>) {
        val process = GlobalScope.async { interventionParameterDbList.map { p -> db.interventionParameterDAO().insertAll(p) } }
        runBlocking { process.await() }
        Log.e("Storing ${interventionParameterDbList.size} parameters", "OK")
    }

    fun getStoredParametersByInterIds(interIds: LongArray): List<InterventionParameterDB> {
        lateinit var res: List<InterventionParameterDB>
        val storedParameters = GlobalScope.async { res = db.interventionParameterDAO().loadAllByInterIds(interIds) }
        runBlocking { storedParameters.await() }

        return res
    }

    fun getStoredParametersWithProducts(): List<InterventionParameterWithProductDB>? {
        lateinit var res: List<InterventionParameterWithProductDB>
        val storedInterventions = GlobalScope.async {
            var product: InterItem? = null
            res = (db.interventionParameterDAO().getAll().map { param ->
                val product: InterItem? = getProduct(param, param.name, 0)
                InterventionParameterWithProductDB(param, product)
            })
        }
        runBlocking { storedInterventions.await() }

        return res
    }

    fun getProduct(param: InterventionParameterDB, name: String, i: Int): InterItem? {
        var product: InterItem? = null
        when (name) {
            "land_parcel" -> {
                val tmp = db.landParcelDAO().loadAllByIds(longArrayOf(param.productId))
                if (tmp.isNotEmpty()) product = LandParcel.createFromLandParcelDB(tmp[0])
            }
            "equipment" -> {
                val tmp = db.equipmentDAO().loadAllByIds(longArrayOf(param.productId))
                if (tmp.isNotEmpty()) product = Equipment.createFromEquipmentDB(tmp[0])
            }
            "animal", "matter" -> {
                val tmp = db.matterDAO().loadAllByIds(longArrayOf(param.productId))
                if (tmp.isNotEmpty()) product = Matter.createFromMatterDB(tmp[0])
            }
            "doer", "worker" -> {
                val tmp = db.workerDAO().loadAllByIds(longArrayOf(param.productId))
                if (tmp.isNotEmpty()) product = Worker.createFromWorkerDB(tmp[0])
            }
            "plant" -> {
                val tmp = db.plantDAO().loadAllByIds(longArrayOf(param.productId))
                if (tmp.isNotEmpty()) product = Plant.createFromPlantDB(tmp[0])
            }
            "output" -> {
                val tmp = db.variantDAO().loadAllByIds(longArrayOf(param.productId))
                if (tmp.isNotEmpty()) product = Variant.createFromVariantDB(tmp[0])
            }
            /*"phyto" -> {
                val tmp = db.phytoDAO().loadAllByIds(longArrayOf(param.productId))
                if (tmp.isNotEmpty()) product = Phyto.createFromPhytoDB(tmp[0])
            }*/
            else -> {
                if (i < 5)
                    return getProduct(param, Onthology.getFamily(name), i+1)
                else
                    return product
            }
        }
        return product
    }

    // ------------ Products ------------ //
    fun storeProducts(interventionProductDbList: List<InterventionProductDB>) {
        val process = GlobalScope.async { interventionProductDbList.map { p -> db.productDAO().insertAll(p) } }
        runBlocking { process.await() }
        Log.e("Storing ${interventionProductDbList.size} products", "OK")
    }

    fun getStoredProductsByParamIds(paramIds: LongArray): List<InterventionProductDB> {
        lateinit var res: List<InterventionProductDB>
        val storedProducts = GlobalScope.async { res = db.productDAO().loadAllByParamIds(paramIds) }
        runBlocking { storedProducts.await() }

        return res
    }

    // ------------ Plants ------------ //
    fun storePlants(plantDBList: List<PlantDB>) {
        val process = GlobalScope.async { plantDBList.map { p -> db.plantDAO().insertAll(p) } }
        runBlocking { process.await() }
        Log.e("Storing ${plantDBList.size} plants", "OK")
    }

    fun getStoredPlants(): List<PlantDB> { return getStoredPlants(null) }

    fun getStoredPlants(variety: String?): List<PlantDB> {
        lateinit var res: List<PlantDB>
        val stored = GlobalScope.async {
            res = if (variety != null)
                db.plantDAO().getByVariety(variety)
            else
                db.plantDAO().getAll()
        }
        runBlocking { stored.await() }

        return res
    }

    fun getStoredPlantById(id: Long): PlantDB? {
        var res: PlantDB? = null
        val stored = GlobalScope.async {
            db.plantDAO().loadAllByIds(longArrayOf(id)).let {
                if (it.isNotEmpty()) res = it[0]
            }
        }
        runBlocking { stored.await() }

        return res
    }

    // ------------ Phytos ------------ //
    fun storePhytos(l: List<PhytoDB>) {
        val process = GlobalScope.async { l.map { p -> db.phytoDAO().insertAll(p) } }
        runBlocking { process.await() }
        Log.e("Storing ${l.size} phytos", "OK")
    }

    fun getStoredPhytos(): List<PhytoDB> {
        lateinit var res: List<PhytoDB>
        val stored = GlobalScope.async {
            res = db.phytoDAO().getAll()
        }
        runBlocking { stored.await() }

        return res
    }

    fun getStoredPhytoById(id: Long): PhytoDB? {
        var res: PhytoDB? = null
        val stored = GlobalScope.async {
            db.phytoDAO().loadAllByIds(longArrayOf(id)).let {
                if (it.isNotEmpty()) res = it[0]
            }
        }
        runBlocking { stored.await() }

        return res
    }


    // ------------ Phytos Usages ------------ //
    fun storePhytoUsages(l: List<PhytoUsageDB>) {
        val process = GlobalScope.async { l.map { p -> db.phytoUsageDAO().insertAll(p) } }
        runBlocking { process.await() }
        Log.e("Storing ${l.size} phyto usages", "OK")
    }

    fun getStoredPhytoUsages(): List<PhytoUsageDB> {
        lateinit var res: List<PhytoUsageDB>
        val stored = GlobalScope.async {
            res = db.phytoUsageDAO().getAll()
        }
        runBlocking { stored.await() }

        return res
    }

    fun getStoredPhytoUsageById(id: String): PhytoUsageDB? {
        var res: PhytoUsageDB? = null
        val stored = GlobalScope.async {
            db.phytoUsageDAO().loadAllByIds(arrayOf(id)).let {
                if (it.isNotEmpty()) res = it[0]
            }
        }
        runBlocking { stored.await() }

        return res
    }

    fun getStoredPhytoUsageByProductId(productId: Long): List<PhytoUsageDB> {
        var res = ArrayList<PhytoUsageDB>()
        val stored = GlobalScope.async {
            res.addAll(db.phytoUsageDAO().loadAllByProductId(productId))
        }
        runBlocking { stored.await() }

        return res
    }


    // ------------ Workers ------------ //
    fun storeWorker(workerDBList: List<WorkerDB>) {
        val process = GlobalScope.async { workerDBList.map { w -> db.workerDAO().insertAll(w) } }
        runBlocking { process.await() }
        Log.e("Storing ${workerDBList.size} plants", "OK")
    }

    fun getStoredWorkers(): List<WorkerDB> { return getStoredWorkers(null) }

    fun getStoredWorkers(variety: String?): List<WorkerDB> {
        lateinit var res: List<WorkerDB>
        val stored = GlobalScope.async {
            res = if (variety != null)
                db.workerDAO().getByVariety(variety)
            else
                db.workerDAO().getAll()
        }
        runBlocking { stored.await() }

        return res
    }

    fun getStoredWorkerById(id: Long): WorkerDB? {
        var res: WorkerDB? = null
        val stored = GlobalScope.async {
            db.workerDAO().loadAllByIds(longArrayOf(id)).let { if (it.isNotEmpty()) res = it[0] }
        }
        runBlocking { stored.await() }

        return res
    }

    // ------------ Equipments ------------ //
    fun storeEquipments(equipmentDBList: List<EquipmentDB>) {
        val process = GlobalScope.async { equipmentDBList.map { e -> db.equipmentDAO().insertAll(e) } }
        runBlocking { process.await() }
        Log.e("Storing ${equipmentDBList.size} equipment", "OK")
    }

    fun getStoredEquipments(): List<EquipmentDB> { return getStoredEquipments(null) }

    fun getStoredEquipments(variety: String?): List<EquipmentDB> {
        lateinit var res: List<EquipmentDB>
        val storedEquipments = GlobalScope.async {
            res = if (variety != null)
                db.equipmentDAO().getbyVariety(variety)
            else
                db.equipmentDAO().getAll()
        }
        runBlocking { storedEquipments.await() }

        return res
    }

    fun getStoredEquipmentById(id: Long): EquipmentDB? {
        var res: EquipmentDB? = null
        val stored = GlobalScope.async {
            db.equipmentDAO().loadAllByIds(longArrayOf(id)).let { if (it.isNotEmpty()) res = it[0] }
        }
        runBlocking { stored.await() }

        return res
    }

    // ------------ Animals ------------ //
    fun storeAnimal(l: List<AnimalDB>) {
        val process = GlobalScope.async { l.map { db.animalDAO().insertAll(it) } }
        runBlocking { process.await() }
        Log.e("Storing ${l.size} animal", "OK")
    }

    fun getStoredAnimals(): List<AnimalDB> { return getStoredAnimals(null) }

    fun getStoredAnimals(variety: String?): List<AnimalDB> {
        lateinit var res: List<AnimalDB>
        val s = GlobalScope.async {
            res = if (variety != null)
                db.animalDAO().getbyVariety(variety)
            else
                db.animalDAO().getAll()
        }
        runBlocking { s.await() }

        return res
    }

    fun getStoredAnimalById(id: Long): AnimalDB? {
        var res: AnimalDB? = null
        val stored = GlobalScope.async {
            db.animalDAO().loadAllByIds(longArrayOf(id)).let { if (it.isNotEmpty()) res = it[0] }
        }
        runBlocking { stored.await() }

        return res
    }

    // ------------ Land Parcels ------------ //
    fun storeLandParcels(landParcelDBList: List<LandParcelDB>) {
        val process = GlobalScope.async { landParcelDBList.map { lp -> db.landParcelDAO().insertAll(lp) } }
        runBlocking { process.await() }
        Log.e("Storing ${landParcelDBList.size} land parcel", "OK")
    }

    fun getStoredLandParcels(): List<LandParcelDB> { return getStoredLandParcels(null) }

    fun getStoredLandParcels(variety: String?): List<LandParcelDB> {
        lateinit var res: List<LandParcelDB>
        val storedLandParcels = GlobalScope.async {
            res = if (variety != null)
                db.landParcelDAO().getByVariety(variety)
            else
                db.landParcelDAO().getAll()
        }
        runBlocking { storedLandParcels.await() }

        return res
    }

    fun getStoredLandParcelById(id: Long): LandParcelDB? {
        var res: LandParcelDB? = null
        val stored = GlobalScope.async {
            db.landParcelDAO().loadAllByIds(longArrayOf(id)).let { if (it.isNotEmpty()) res = it[0] }
        }
        runBlocking { stored.await() }

        return res
    }

    // ------------ Building Divisions ------------ //
    fun storeBuildingDivisions(buildingDivisionDBList: List<BuildingDivisionDB>) {
        val process = GlobalScope.async { buildingDivisionDBList.map { bd -> db.buildingDivisionDAO().insertAll(bd) } }
        runBlocking { process.await() }
        Log.e("Storing ${buildingDivisionDBList.size} builDdivision", "OK")
    }

    fun getStoredBuildingDivisions(): List<BuildingDivisionDB> { return getStoredBuildingDivisions(null) }

    fun getStoredBuildingDivisions(variety: String?): List<BuildingDivisionDB> {
        lateinit var res: List<BuildingDivisionDB>
        val storedBuildingDivisions = GlobalScope.async {
            res = if (variety != null)
                db.buildingDivisionDAO().getByVariety(variety)
            else
                db.buildingDivisionDAO().getAll()
        }
        runBlocking { storedBuildingDivisions.await() }

        return res
    }

    fun getStoredBuildingDivisionById(id: Long): BuildingDivisionDB? {
        var res: BuildingDivisionDB? = null
        val stored = GlobalScope.async {
            db.buildingDivisionDAO().loadAllByIds(longArrayOf(id)).let { if (it.isNotEmpty()) res = it[0] }
        }
        runBlocking { stored.await() }

        return res
    }

    // ------------ Matters ------------ //
    fun storeMatters(matterDBList: List<MatterDB>) {
        val process = GlobalScope.async { matterDBList.map { m -> db.matterDAO().insertAll(m) } }
        runBlocking { process.await() }
        Log.e("Storing ${matterDBList.size} matter", "OK")
    }

    fun getStoredMatters(): List<MatterDB> { return getStoredMatters(null) }

    fun getStoredMatters(variety: String?): List<MatterDB> {
        lateinit var res: List<MatterDB>
        val stored = GlobalScope.async {
            res = if (variety != null)
                db.matterDAO().getByVariety(variety)
            else
                db.matterDAO().getAll()
        }
        runBlocking { stored.await() }

        return res
    }

    fun getStoredMatterById(id: Long): MatterDB? {
        var res: MatterDB? = null
        val stored = GlobalScope.async {
            db.matterDAO().loadAllByIds(longArrayOf(id)).let { if (it.isNotEmpty()) res = it[0] }
        }
        runBlocking { stored.await() }

        return res
    }

    // ------------ Variants ------------ //
    fun storeVariants(variantsDBList: List<VariantDB>) {
        val process = GlobalScope.async { variantsDBList.map { v -> db.variantDAO().insertAll(v) } }
        runBlocking { process.await() }
        Log.e("Storing ${variantsDBList.size} variant", "OK")
    }

    fun getStoredVariants(): List<VariantDB> { return getStoredVariants(null) }

    fun getStoredVariants(variety: String?): List<VariantDB> {
        lateinit var res: List<VariantDB>
        val stored = GlobalScope.async {
            res = if (variety != null)
                db.variantDAO().getByVariety(variety)
            else
                db.variantDAO().getAll()
        }
        runBlocking { stored.await() }

        return res
    }

    fun getStoredVariantById(id: Long): VariantDB? {
        var res: VariantDB? = null
        val stored = GlobalScope.async {
            db.variantDAO().loadAllByIds(longArrayOf(id)).let { if (it.isNotEmpty()) res = it[0] }
        }
        runBlocking { stored.await() }

        return res
    }

    // --------------------- Procedures ------------------- //
    fun storeProcedures(procedureList: List<Procedure>) {
        val process =  GlobalScope.async {
            procedureList.map { p ->
                val procedureId = db.procedureDAO().insert(p.adaptForDb())
                p.parameters?.let { params ->
                    params.group?.let { it.forEach { storeParametersByType(it, procedureId, it.name!!)  } }
                    storeParametersByType(GenericProcedureParameter("", params.target, params.output, params.input, params.doer, params.tool), procedureId, "")
                }
            }
        }
        runBlocking { process.await() }
        Log.e("Storing ${procedureList.size} procedures", "OK")
    }

    fun destroyProcedures() {
        val process =  GlobalScope.async {
            db.procedureDAO().deleteAll()
            db.procedureHandlerDAO().deleteAll()
            db.procedureParameterDAO().deleteAll()
            db.procedureProductParameterDAO().deleteAll()
        }
        runBlocking { process.await() }
        Log.e("Clearing all procedures", "OK")
    }

    private fun storeParametersByType(genericProcedureParameter: GenericProcedureParameter, procedureId: Long, isGroup: String) {
        storeProcedureParameter(genericProcedureParameter.input, ProcedureParameterTypes.INPUT, procedureId, isGroup)
        storeProcedureParameter(genericProcedureParameter.output, ProcedureParameterTypes.OUTPUT, procedureId, isGroup)
        storeProcedureParameter(genericProcedureParameter.target, ProcedureParameterTypes.TARGET, procedureId, isGroup)
        storeProcedureParameter(genericProcedureParameter.doer, ProcedureParameterTypes.DOER, procedureId, isGroup)
        storeProcedureParameter(genericProcedureParameter.tool, ProcedureParameterTypes.TOOL, procedureId, isGroup)
    }

    private fun storeProcedureParameter(l: List<GenericProductParameter>?, type: ProcedureParameterTypes, procedureId: Long, isGroup: String) {
        l?.let { parameterList ->
            val insertedParamId = db.procedureParameterDAO().insert(ProcedureParameterDB(isGroup, type.typeName, procedureId))
            parameterList.map { genericProductParameter ->
                val insertedProductId = db.procedureProductParameterDAO().insert(genericProductParameter.adaptForDb(insertedParamId))
                genericProductParameter.handler?.let { handlerList ->
                    handlerList.map {  handler ->
                        db.procedureHandlerDAO().insert(handler.adaptForDb(insertedProductId))
                    }
                }
            }
        }
    }

    fun getAllProcedures(): List<ProcedureDB> {
        val res = ArrayList<ProcedureDB>()
        val process =  GlobalScope.async {
            res.addAll(db.procedureDAO().getAll())
        }
        runBlocking { process.await() }
        return res
    }

    fun getProceduresCategories(): List<String> {
        val res = ArrayList<String>()
        val process =  GlobalScope.async {
            val tmp = db.procedureDAO().getProcedureCategories()

            tmp.map { it.split(", ").map { c ->
                if (!res.contains(c))
                    res.add(c)
            } }
        }
        runBlocking { process.await() }
        return res
    }

    fun getProcedureByName(procedureName: String): ProcedureDB {
        lateinit var res: ProcedureDB
        val process =  GlobalScope.async {
            res = db.procedureDAO().getProcedureByName(procedureName)[0]
        }
        runBlocking { process.await() }
        return res
    }

    fun getParametersByProcedureId(procedureId: Long): List<ProcedureParameterDB> {
        lateinit var res: List<ProcedureParameterDB>
        val process =  GlobalScope.async {
            res = db.procedureParameterDAO().getParametersByProcedureId(procedureId)
        }
        runBlocking { process.await() }
        return res
    }

    fun getProductsByParameterId(paramId: Long): List<ProcedureProductDB> {
        lateinit var res: List<ProcedureProductDB>
        val process =  GlobalScope.async {
            res = db.procedureProductParameterDAO().getByParameterId(paramId)
        }
        runBlocking { process.await() }
        return res
    }

    fun getHandlersByProductId(productId: Long): List<ProcedureHandlerDB> {
        lateinit var res: List<ProcedureHandlerDB>
        val process =  GlobalScope.async {
            res = db.procedureHandlerDAO().getByProductId(productId)
        }
        runBlocking { process.await() }
        return res
    }
}