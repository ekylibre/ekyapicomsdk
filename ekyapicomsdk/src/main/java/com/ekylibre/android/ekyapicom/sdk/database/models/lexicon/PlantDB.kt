package com.ekylibre.android.ekyapicom.sdk.database.models.lexicon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PlantDB (
    @PrimaryKey val plant_id: Long,
    @ColumnInfo(name = "plant_name") val name: String,
    @ColumnInfo(name = "plant_netsurfacearea") val netSurfaceArea: String,
    @ColumnInfo(name = "plant_variety") val variety: String,
    @ColumnInfo(name = "plant_activity_id") val activityId: Long,
    @ColumnInfo(name = "plant_activity_name") val activityName: String,
    @ColumnInfo(name = "plant_production_start") val productionStartedOn: String,
    @ColumnInfo(name = "plant_production_stop") val productionStoppedOn: String,
    @ColumnInfo(name = "plant_shape_svg") val shapeSvg: String
)