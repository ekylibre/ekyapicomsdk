package com.ekylibre.android.ekyapicom.sdk.database.models.interventions

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.SyncState
import java.util.Date

@Entity
data class InterventionDB (
    @PrimaryKey val i_id: Long,
    @ColumnInfo(name = "i_nature") val nature: String,
    @ColumnInfo(name = "i_procedure") val procedure: String,
    @ColumnInfo(name = "i_number") val number: Long,
    @ColumnInfo(name = "i_name") val name: String,
    @ColumnInfo(name = "i_startDate") val startDate: Long,
    @ColumnInfo(name = "i_stopDate") val stopDate: Long,
    @ColumnInfo(name = "i_description") val description: String,
    @ColumnInfo(name = "i_cost") var totalCost: Double,
    @ColumnInfo(name = "i_account_id") var accountId: Long,
    @ColumnInfo(name = "i_state") var state: Int
)