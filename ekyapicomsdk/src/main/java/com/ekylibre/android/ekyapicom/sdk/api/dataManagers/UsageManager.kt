package com.ekylibre.android.ekyapicom.sdk.api.dataManagers

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.api.APICommunicationModule
import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.ekylibre.android.ekyapicom.sdk.api.ApiInterface
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.PaginatedUsageResponse
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.PhytoUsage
import retrofit2.Call
import retrofit2.Response

class UsageManager(var context: Context, var ekyDbSDK: EkyDbSDK): APICommunicationModule {
    override val errorSharedPrefsKey: String
        get() = "sync_error"

    init {}

    override fun onResponseCallback(r: Response<APIResponse>) {
        if (r.body() != null && r.isSuccessful) {
            val dataToStore: List<PhytoUsage> = (r.body()!! as PaginatedUsageResponse).data
            ekyDbSDK.storePhytoUsages(dataToStore.map { d -> d.adaptForDb() })

            Utils.saveInSharedPreferences(
                mapOf("last_phytos_usages_sync" to Utils.getTodaysDate()),
                context
            )
        }
    }

    fun pull() {
        pullOnePage()
    }

    private fun pullOnePage() {
        val modifiedSince = Utils.readStringInSharedPreferences("last_phytos_usages_sync", context)
        val response =
            if (modifiedSince == "")
                ApiInterface.get(context)?.getPhytoUsagesAsync(null)
            else ApiInterface.get(context)?.getPhytoUsagesAsync(modifiedSince)
        val apiUrl = Utils.readStringInSharedPreferences("api_url", context)
        response?.let { enqueueResponse(apiUrl, response as Call<APIResponse>, context) }
    }
}