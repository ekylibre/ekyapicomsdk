package com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ParamAttributes(var product_id: Long?,
                      var variant_id: Long?,
                      var reference_name: String,
                      var batch_number: String?,
                      var quantity_value: Double?,
                      var quantity_handler: String?,
                      var quantity_population: Int?,
                      var new_name: String?,
                      var identification_number: Long?,
                      var readings_attributes: List<ReadingAttribute>?) {

    fun getPreview(): String {
        product_id?.let { return it.toString() }
        variant_id?.let { return it.toString() }
        return ""
    }
}