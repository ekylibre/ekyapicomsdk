package com.ekylibre.android.ekyapicom.sdk.api.dataManagers

import android.content.Context
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.api.APICommunicationModule
import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.ekylibre.android.ekyapicom.sdk.api.ApiInterface
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.Plant
import retrofit2.Call
import retrofit2.Response

class PlantManager(var context: Context, var ekyDbSDK: EkyDbSDK): APICommunicationModule {

    override val errorSharedPrefsKey: String
        get() = "sync_error"

    override fun onResponseCallback(r: Response<APIResponse>) {
        if (r.body() != null && r.isSuccessful) {
            val plantsToStore: List<Plant> = r.body()!! as List<Plant>
            ekyDbSDK.storePlants(plantsToStore.map { p -> p.adaptForDb() })

            Utils.saveInSharedPreferences(mapOf("last_plants_sync" to Utils.getTodaysDate()), context)
        }
    }

    fun pull() {
        val modifiedSince = Utils.readStringInSharedPreferences("last_plants_sync", context)
        val response = if (modifiedSince == "") ApiInterface.get(context)?.getPlantsAsync(null) else ApiInterface.get(context)?.getPlantsAsync(modifiedSince)
        val apiUrl = Utils.readStringInSharedPreferences("api_url", context)
        response?.let { enqueueResponse(apiUrl, response as Call<APIResponse>, context) }
    }
}