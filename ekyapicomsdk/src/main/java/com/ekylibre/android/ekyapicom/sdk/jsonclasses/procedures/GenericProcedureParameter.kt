package com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class GenericProcedureParameter(val name: String?,
                                val target: MutableList<GenericProductParameter>?,
                                val output: MutableList<GenericProductParameter>?,
                                val input: MutableList<GenericProductParameter>?,
                                val doer: MutableList<GenericProductParameter>?,
                                val tool: MutableList<GenericProductParameter>?) {
}