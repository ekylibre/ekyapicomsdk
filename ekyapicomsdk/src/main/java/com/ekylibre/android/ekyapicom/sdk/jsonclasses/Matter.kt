package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.MatterDB
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class Matter(var id: Long, var name: String, var number: String, var population: String, var variety: String, var abilities: MutableList<String>, var born_at: Date, var derivative_of: String?, var unit_name: String, var container_name: String?): InterItem {

    @Transient
    private var quantity = 0.0
    @Transient
    private var itemUnit = ""

    fun adaptForDb(): MatterDB {
        return MatterDB(id, name, number, population, variety, abilities.joinToString(), born_at.time, derivative_of ?: "", unit_name, container_name ?: "")
    }

    companion object {
        fun createFromMatterDB(m: MatterDB): Matter {
            return Matter(m.m_id, m.name, m.number, m.population, m.variety, m.abilities.split(", ").toMutableList(), Date(m.born_at), m.derivative_of, m.unit_name, m.container_name)
        }
    }

    override fun setQuantity(q: Double?, unit: String?) {
        q?.let { this.quantity = q }
        unit?.let { this.itemUnit = unit }
    }

    override fun getUnit(): String { return if (this.itemUnit.isEmpty()) "population" else this.itemUnit }
    override fun getQuantity(): Double { return this.quantity }
    override fun getHours(): Double? { return null }
    override fun getItemVariety(): String { return this.variety }
    override fun getItemAbilities(): List<String> { return this.abilities }
    override fun getStartDate(): Date? { return this.born_at }
    override fun getEndDate(): Date? { return null }
    override fun getDerivative(): String? { return this.derivative_of }
    override fun getSurfaceArea(): NetSurfaceArea? { return null }
    override fun getSvgShape(): String? { return null }
    override fun getProductId(): Long { return this.id }
    override fun getProductName(): String { return this.name }
}