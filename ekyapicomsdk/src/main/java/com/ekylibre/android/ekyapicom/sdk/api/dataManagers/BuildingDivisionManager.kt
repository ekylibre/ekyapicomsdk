package com.ekylibre.android.ekyapicom.sdk.api.dataManagers

import android.content.Context
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.api.APICommunicationModule
import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.ekylibre.android.ekyapicom.sdk.api.ApiInterface
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.BuildingDivision
import retrofit2.Call
import retrofit2.Response

class BuildingDivisionManager(var context: Context, var ekyDbSDK: EkyDbSDK): APICommunicationModule {

    override val errorSharedPrefsKey: String
        get() = "sync_error"

    override fun onResponseCallback(r: Response<APIResponse>) {
        if (r.body() != null && r.isSuccessful) {
            val buildingDivisionsToStore: List<BuildingDivision> = r.body()!! as List<BuildingDivision>
            ekyDbSDK.storeBuildingDivisions(buildingDivisionsToStore.map { bd -> bd.adaptForDb() })

            Utils.saveInSharedPreferences(mapOf("last_building_divisions_sync" to Utils.getTodaysDate()), context)
        }
    }

    fun pull() {
        val modifiedSince = Utils.readStringInSharedPreferences("last_building_divisions_sync", context)
        val response = if (modifiedSince == "") ApiInterface.get(context)?.getBuildingDivisionsAsync(null) else ApiInterface.get(context)?.getBuildingDivisionsAsync(modifiedSince)
        val apiUrl = Utils.readStringInSharedPreferences("api_url", context)
        response?.let { enqueueResponse(apiUrl, it as Call<APIResponse>, context) }
    }
}