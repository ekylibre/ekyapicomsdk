package com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class ProvidersData(var zero_id: Long)