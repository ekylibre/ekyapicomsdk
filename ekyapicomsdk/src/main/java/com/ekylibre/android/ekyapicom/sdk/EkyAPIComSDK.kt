package com.ekylibre.android.ekyapicom.sdk

import android.content.Context
import android.util.Log
import com.ekylibre.android.ekyapicom.sdk.api.authentication.AuthModuleInterface
import com.ekylibre.android.ekyapicom.sdk.api.authentication.OAuthModule
import com.ekylibre.android.ekyapicom.sdk.api.authentication.SimpleAuthModule
import com.ekylibre.android.ekyapicom.sdk.api.dataManagers.*
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import com.ekylibre.android.ekyapicom.sdk.database.models.AccountDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionToSendDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionWithParameterAndProductDB
import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureDB
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.*
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionResponse
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions.InterventionToSend
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.Phyto
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos.PhytoUsage
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.GenericProductParameter
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.Procedure
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.ProcedureParameterWithGroup
import java.lang.NumberFormatException
import kotlin.collections.ArrayList
import kotlin.concurrent.thread

class EkyAPIComSDK() {
    private var isOAuth = false
    private lateinit var context: Context
    private var authModule: AuthModuleInterface? = null

    private lateinit var ekyDbSDK: EkyDbSDK

    constructor(api_url: String, client_id: String, client_secret: String, c: Context) : this() {
        Log.e("EkyAPI", "Creating object for OAuth...")
        context = c
        isOAuth = true
        authModule = OAuthModule(api_url, client_id, client_secret, c)
        ekyDbSDK = EkyDbSDK(c)
    }

    constructor(api_url: String, c: Context) : this() {
        Log.e("EkyAPI", "Creating object for Simple Auth...")

        context = c
        ekyDbSDK = EkyDbSDK(c)
        ekyDbSDK.build()
        authModule = if (!api_url.startsWith("http"))
            SimpleAuthModule("https://$api_url", c, ekyDbSDK)
        else SimpleAuthModule(api_url, c, ekyDbSDK)
    }

    constructor(c: Context): this() {
        Log.e("EkyAPI", "Recreating object...")
        context = c
        ekyDbSDK = EkyDbSDK(c)
        ekyDbSDK.build()
    }

    fun init() {
        Log.e("EkyAPI", "Initialisation...")
        val accountExists = ekyDbSDK.getStoredAccounts(this.authModule!!.apiUrl, this.authModule!!.username)//.isNotEmpty()
        if (accountExists.size > 0)
            Utils.saveInSharedPreferences("authentication_error", this.context.getString(R.string.account_already_stored) + "@" + Utils.getTodaysDate(), context)
        else authModule!!.init()
    }

    fun setCredentials(username: String, password: String) {
        Log.e("EkyAPI", "Setting credentials...")
        authModule!!.setCredentials(username, password)
    }

    fun getInterItemByIdFromDB(id: Long): InterItem? {
        ekyDbSDK.getStoredLandParcelById(id)?.let { return LandParcel.createFromLandParcelDB(it) }
        ekyDbSDK.getStoredPlantById(id)?.let { return Plant.createFromPlantDB(it) }
        ekyDbSDK.getStoredBuildingDivisionById(id)?.let { return BuildingDivision.createFromBuildingDivisionDB(it) }
        return null
    }

    // -------------- Plants -------------- //
    fun getPlantsFromAPI() {
        Log.e("EkyAPI", "Pulling plants from API...")
        thread { PlantManager(context, ekyDbSDK).pull() }
    }

    fun getPlantsFromDB(): List<Plant> {
        Log.e("EkyDB", "Pulling plants from local Database...")
        return ekyDbSDK.getStoredPlants().map { p -> Plant.createFromPlantDB(p) }
    }

    fun getPlantsByVarietyFromDB(variety: String): List<Plant> {
        Log.e("EkyDB", "Pulling plants from local Database...")
        return ekyDbSDK.getStoredPlants(variety).map { e -> Plant.createFromPlantDB(e) }
    }

    // -------------- Phytos -------------- //
    fun getPhytosFromAPI() {
        Log.e("EkyAPI", "Pulling phytos from API...")
        thread { PhytoManager(context, ekyDbSDK).pull() }
    }

    fun getPhytosFromDB(): List<Phyto> {
        Log.e("EkyDB", "Pulling phytos from local Database...")
        return ekyDbSDK.getStoredPhytos().map { p -> Phyto.createFromPhytoDB(p) }
    }

    fun getPhytoFromDB(phytoId: Long): Phyto? {
        Log.e("EkyDB", "Pulling phytos from local Database...")
        ekyDbSDK.getStoredPhytoById(phytoId)?.let { return Phyto.createFromPhytoDB(it) }
        return null
    }

    // -------------- Phytos usages -------------- //
    fun getPhytoUsagesFromAPI() {
        Log.e("EkyAPI", "Pulling phyto usages from API...")
        thread { UsageManager(context, ekyDbSDK).pull() }
    }

    fun getPhytoUsagesFromDB(): List<PhytoUsage> {
        Log.e("EkyDB", "Pulling phytos from local Database...")
        return ekyDbSDK.getStoredPhytoUsages().map { p -> PhytoUsage.createFromPhytoUsageDB(p) }
    }

    fun getPhytoUsagesFromDB(phytoId: Long): List<PhytoUsage> {
        Log.e("EkyDB", "Pulling phytos from local Database...")
        return ekyDbSDK.getStoredPhytoUsageByProductId(phytoId).map { p -> PhytoUsage.createFromPhytoUsageDB(p) }
    }

    // -------------- Workers -------------- //
    fun getWorkersFromAPI() {
        Log.e("EkyAPI", "Pulling workers from API...")
        thread { WorkerManager(context, ekyDbSDK).pull() }
    }

    fun getWorkersFromDB(): List<Worker> {
        Log.e("EkyDB", "Pulling workers from local Database...")
        return ekyDbSDK.getStoredWorkers().map { w -> Worker.createFromWorkerDB(w) }
    }

    fun getWorkersByVarietyFromDB(variety: String): List<Worker> {
        Log.e("EkyDB", "Pulling equipments from local Database...")
        return ekyDbSDK.getStoredWorkers(variety).map { e -> Worker.createFromWorkerDB(e) }
    }

    // -------------- Interventions -------------- //
    fun getInterventionsFromAPI() {
        Log.e("EkyAPI", "Pulling interventions from API...")
        thread { InterventionManager(context, ekyDbSDK).pullInterventions() }
    }

    fun getRemoteInterventionsFromDB(): List<InterventionResponse> {
        Log.e("EkyDB", "Pulling remote interventions from local Database...")
        return ekyDbSDK.getDetailedStoredInterventions().map { i -> InterventionResponse.createFromInterventionDB(i) }
    }

    fun getRemoteInterventionFromDB(id: Long): InterventionWithParameterAndProductDB {
        Log.e("EkyDB", "Pulling remote intervention #$id from local Database...")
        return ekyDbSDK.getDetailedStoredIntervention(id)
    }

    fun getLocalInterventionsFromDB(): List<InterventionToSend> {
        Log.e("EkyDB", "Pulling local interventions from local Database...")
        val res = ArrayList<InterventionToSend>()
        ekyDbSDK.getInterventionsToSend().map { i -> InterventionToSend.fromJson(i.jsonBody, i.id)?.let { res.add(it) } }
        return res
    }

    fun storeIntervention(i: InterventionToSend) {
        ekyDbSDK.storeLocalJSONIntervention(i.toJson())
    }

    fun pushIntervention(i: InterventionToSend) {
        Log.e("EkyAPI", "Pushing intervention to API...")
        thread { InterventionManager(context, ekyDbSDK).pushIntervention(i) }
    }

    fun pushLocalInterventions() {
        Log.e("EkyAPI", "Pushing interventions to API...")
        thread {
            getLocalInterventionsFromDB().map {
                InterventionManager(context, ekyDbSDK).pushIntervention(it)
                val accountId = com.ekylibre.android.ekyapicom.sdk.database.Utils.readStringInSharedPreferences("account_id", context)
                ekyDbSDK.deleteLocalJSONIntervention(InterventionToSendDB(it.id, accountId.toLong(), it.toJson()))
            }
        }
    }

    fun deleteLocalIntervention(inter: InterventionToSend) {
        Log.e("EkyAPI", "Deleting intervention from DB...")
        thread {
            val accountId = com.ekylibre.android.ekyapicom.sdk.database.Utils.readStringInSharedPreferences("account_id", context)
            ekyDbSDK.deleteLocalJSONIntervention(InterventionToSendDB(inter.id, accountId.toLong(), inter.toJson()))
        }
    }

    fun destroyInterventionsInDB() {
        ekyDbSDK.destroyInterventions()
    }

    // -------------- Accounts -------------- //
    fun getAccountsFromDB(): List<AccountDB> {
        Log.e("EkyDB", "Pulling accounts from local Database...")
        return ekyDbSDK.getStoredAccounts()
    }

    fun getCurrentAccount(): AccountDB? {
        Log.e("EkyDB", "Pulling current account from local Database...")
        return try {
            ekyDbSDK.getStoredAccounts(intArrayOf(Utils.readStringInSharedPreferences("account_id", context).toInt()))
        } catch (e: NumberFormatException) {
            null
        }
    }

    fun getInactiveAccountsFromDb(): List<AccountDB> {
        Log.e("EkyDB", "Pulling inactive account from local Database...")
        return try {
            ekyDbSDK.getInactiveAccounts(Utils.readStringInSharedPreferences("account_id", context).toInt())
        } catch (e: NumberFormatException) {
            ArrayList()
        }
    }

    fun switchCurrentAccount(accountId: Int) {
        Log.e("EkyDB", "Pulling account from local Database...")
        val accounts = ekyDbSDK.getStoredAccounts(intArrayOf(accountId))
        val valuesToStore = mapOf("access_token" to accounts.token, "api_url" to accounts.api_url, "username" to accounts.username, "account_id" to accounts.a_id.toString())
        Utils.saveInSharedPreferences(valuesToStore, this.context)
    }

    fun clearCurrentAccount() {
        Utils.saveInSharedPreferences("access_token", "", context)
        Utils.saveInSharedPreferences("account_id", "", context)
    }

    fun deleteAccount(accountDb: AccountDB) {
        Log.e("EkyDB", "Deleting account from local Database...")
        ekyDbSDK.deleteAccount(accountDb)
    }

    // -------------- Equipments -------------- //
    fun getEquipmentsFromAPI() {
        Log.e("EkyAPI", "Pulling equipments from API...")
        thread { EquipmentManager(context, ekyDbSDK).pull() }
    }

    fun getEquipmentsFromDB(): List<Equipment> {
        Log.e("EkyDB", "Pulling equipments from local Database...")
        return ekyDbSDK.getStoredEquipments().map { e -> Equipment.createFromEquipmentDB(e) }
    }

    fun getEquipmentsByVarietyFromDB(variety: String): List<Equipment> {
        Log.e("EkyDB", "Pulling equipments from local Database...")
        return ekyDbSDK.getStoredEquipments(variety).map { e -> Equipment.createFromEquipmentDB(e) }
    }

    // -------------- Animals -------------- //
    fun getAnimalsFromAPI() {
        Log.e("EkyAPI", "Pulling animals from API...")
        thread { AnimalManager(context, ekyDbSDK).pull() }
    }

    fun getAnimalsFromDB(): List<Animal> {
        Log.e("EkyDB", "Pulling animals from local Database...")
        return ekyDbSDK.getStoredAnimals().map { Animal.createFromDB(it) }
    }

    fun getAnimalsByVarietyFromDB(variety: String): List<Animal> {
        Log.e("EkyDB", "Pulling animals from local Database...")
        return ekyDbSDK.getStoredAnimals(variety).map { Animal.createFromDB(it) }
    }

    // -------------- Land Parcels -------------- //
    fun getLandParcelsFromAPI() {
        Log.e("EkyAPI", "Pulling land parcels from API...")
        thread { LandParcelManager(context, ekyDbSDK).pull() }
    }

    fun getLandParcelsFromDB(): List<LandParcel> {
        Log.e("EkyDB", "Pulling land parcels from local Database...")
        return ekyDbSDK.getStoredLandParcels().map { lp -> LandParcel.createFromLandParcelDB(lp) }
    }

    fun getLandParcelsByVarietyFromDB(variety: String): List<LandParcel> {
        Log.e("EkyDB", "Pulling land parcels from local Database...")
        return ekyDbSDK.getStoredLandParcels(variety).map { lp -> LandParcel.createFromLandParcelDB(lp) }
    }

    // -------------- Building Division -------------- //
    fun getBuildingDivisionsFromAPI() {
        Log.e("EkyAPI", "Pulling building divisions from API...")
        thread { BuildingDivisionManager(context, ekyDbSDK).pull() }
    }

    fun getBuildingDivisionsFromDB(): List<BuildingDivision> {
        Log.e("EkyDB", "Pulling building divisions from local Database...")
        return ekyDbSDK.getStoredBuildingDivisions().map { bd -> BuildingDivision.createFromBuildingDivisionDB(bd) }
    }

    fun getBuildingDivisionsByVarietyFromDB(variety: String): List<BuildingDivision> {
        Log.e("EkyDB", "Pulling building divisions from local Database...")
        return ekyDbSDK.getStoredBuildingDivisions(variety).map { lp -> BuildingDivision.createFromBuildingDivisionDB(lp) }
    }

    // -------------- Matters -------------- //
    fun getMattersFromAPI() {
        Log.e("EkyAPI", "Pulling matters from API...")
        thread { MatterManager(context, ekyDbSDK).pull() }
    }

    fun getMattersFromDB(): List<Matter> {
        Log.e("EkyDB", "Pulling matters from local Database...")
        return ekyDbSDK.getStoredMatters().map { m -> Matter.createFromMatterDB(m) }
    }

    fun getMattersByVarietyFromDB(variety: String): List<Matter> {
        Log.e("EkyDB", "Pulling building divisions from local Database...")
        return ekyDbSDK.getStoredMatters(variety).map { lp -> Matter.createFromMatterDB(lp) }
    }

    // -------------- Variants -------------- //
    fun getVariantsFromAPI() {
        Log.e("EkyAPI", "Pulling variants from API...")
        thread { VariantManager(context, ekyDbSDK).pull() }
    }

    fun getVariantsFromDB(): List<Variant> {
        Log.e("EkyDB", "Pulling variants from local Database...")
        return ekyDbSDK.getStoredVariants().map { v -> Variant.createFromVariantDB(v) }
    }

    fun getVariantsByVarietyFromDB(variety: String): List<Variant> {
        Log.e("EkyDB", "Pulling variants from local Database...")
        return ekyDbSDK.getStoredVariants(variety).map { Variant.createFromVariantDB(it) }
    }

    // ------------------ Procedures -------------------- //
    fun refreshProceduresInDB() {
        destroyProceduresInDB()
        val procedures = Utils.refreshProcedureFiles(context)
        ekyDbSDK.storeProcedures(procedures)
        Log.e("EkyDB", "Procedures refreshed")
    }

    fun destroyProceduresInDB() {
        ekyDbSDK.destroyProcedures()
    }

    fun getProcedureCategories(): List<String> {
        return ekyDbSDK.getProceduresCategories()
    }

    fun getAllSimplifiedProcedures(): List<ProcedureDB> {
        return ekyDbSDK.getAllProcedures()
    }

    fun getDetailedProcedure(procedureName: String): Procedure {
        val procedureDB = ekyDbSDK.getProcedureByName(procedureName)
        val paramsDb = ekyDbSDK.getParametersByProcedureId(procedureDB.id)
        val products = HashMap<Long, ArrayList<GenericProductParameter>>()
        paramsDb.map { param ->
            ekyDbSDK.getProductsByParameterId(param.id).map {
                (products.getOrPut(param.id) { ArrayList() }).add(GenericProductParameter.createFromDB(it, ekyDbSDK.getHandlersByProductId(it.id)))
            }
        }
        return Procedure(procedureDB.name, procedureDB.categories, procedureDB.position, ProcedureParameterWithGroup.createFromProcedureParametersDB(paramsDb, products))
    }

    fun destroy() {
        Log.e("EkyAPI", "Destroying data...")
        authModule!!.destroy()
    }

    fun getCurrentWorkerId(): Long {
        return Utils.readLongInSharedPreferences("eky_worker_id", context)
    }

    companion object {
        fun isUserLogged(context: Context): Boolean {
            return context.getSharedPreferences(context.packageName, 0).getString("access_token", "")!! != ""
        }
    }
}