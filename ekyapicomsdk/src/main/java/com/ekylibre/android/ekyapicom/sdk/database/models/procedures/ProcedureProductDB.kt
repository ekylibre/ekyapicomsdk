package com.ekylibre.android.ekyapicom.sdk.database.models.procedures

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class ProcedureProductDB(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "procedure_product_id") val id: Long,
        @ColumnInfo(name = "procedure_product_name") val name: String,
        @ColumnInfo(name = "procedure_product_filter") val filter: String,
        @ColumnInfo(name = "procedure_product_cardinality") val cardinality: String,
        @ColumnInfo(name = "procedure_parameter_foreign_id") val procedureParameterId: Long
) {
        constructor(name: String, filter: String, cardinality: String, procedureParameterId: Long) : this(0, name, filter, cardinality, procedureParameterId)
}