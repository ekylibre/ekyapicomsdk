package com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures

import com.ekylibre.android.ekyapicom.sdk.database.models.procedures.ProcedureHandlerDB

class HandlerParameter(val name: String?, val indicator: String?, val unit: String?) {
    fun adaptForDb(procedureProductParameterId: Long): ProcedureHandlerDB {
        return ProcedureHandlerDB(name ?: "", indicator ?: "", unit ?: "", procedureProductParameterId)
    }

    companion object {
        fun createFromDB(h: ProcedureHandlerDB): HandlerParameter {
            return HandlerParameter(h.name, h.indicator, h.unit)
        }

        fun extractUnits(list: MutableList<HandlerParameter>?): List<String> {
            val units = ArrayList<String>()
            list?.map { h -> h.unit?.let { units.add(it) } }
            return units
        }

        fun toUnitMap(l: List<HandlerParameter>?): Map<String, String> {
            val m = HashMap<String, String>()
            l?.map { h ->
                h.unit?.let {
                    if (h.name.isNullOrBlank())
                        h.indicator?.let { m[h.unit] = h.indicator }
                    else
                        m[h.unit] = h.name
                }
            }
            return m
        }
    }
}