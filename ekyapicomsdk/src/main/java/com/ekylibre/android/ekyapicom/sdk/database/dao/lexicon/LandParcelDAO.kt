package com.ekylibre.android.ekyapicom.sdk.database.dao.lexicon

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.LandParcelDB

@Dao
interface LandParcelDAO {
    @Query("SELECT * FROM landparceldb")
    fun getAll(): List<LandParcelDB>

    @Query("SELECT * FROM landparceldb WHERE lp_variety LIKE :variety")
    fun getByVariety(variety: String): List<LandParcelDB>

    @Query("SELECT * FROM landparceldb WHERE lp_id IN (:landParcelIds)")
    fun loadAllByIds(landParcelIds: LongArray): List<LandParcelDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg landParcelDB: LandParcelDB)

    @Delete
    fun delete(landParcelDB: LandParcelDB)
}