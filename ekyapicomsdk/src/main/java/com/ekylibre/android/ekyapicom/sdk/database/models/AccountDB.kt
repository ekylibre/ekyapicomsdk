package com.ekylibre.android.ekyapicom.sdk.database.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AccountDB(
    @ColumnInfo(name = "a_username") var username: String,
    @ColumnInfo(name = "a_token") var token: String,
    @ColumnInfo(name = "a_api_url") var api_url: String
) {
    @PrimaryKey(autoGenerate = true) var a_id: Int = 0
    @ColumnInfo(name = "a_worker_eky_id") var worker_eky_id: Long = -1
}