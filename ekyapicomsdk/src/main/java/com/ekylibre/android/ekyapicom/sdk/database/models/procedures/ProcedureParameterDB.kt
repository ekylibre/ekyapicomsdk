package com.ekylibre.android.ekyapicom.sdk.database.models.procedures

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ProcedureParameterDB(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "proc_param_id") val id: Long = 0,
        @ColumnInfo(name = "is_group") var isGroup: String,
        var type: String,
        @ColumnInfo(name = "f_proc_id") var procedureForeignId: Long) {

        constructor(isGroup: String, type: String, procedureForeignId: Long) : this(0, isGroup, type, procedureForeignId)
}