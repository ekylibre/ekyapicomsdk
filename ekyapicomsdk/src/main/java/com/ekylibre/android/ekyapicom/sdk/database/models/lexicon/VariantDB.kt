package com.ekylibre.android.ekyapicom.sdk.database.models.lexicon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class VariantDB(
        @PrimaryKey  @ColumnInfo(name = "variant_id") val id: Long,
        @ColumnInfo(name = "variant_name") val name: String,
        @ColumnInfo(name = "variant_number") val number: String,
        @ColumnInfo(name = "variant_variety") val variety: String,
        @ColumnInfo(name = "variant_derivative_of") val derivativeOf: String,
        @ColumnInfo(name = "variant_abilities") val abilities: String
)