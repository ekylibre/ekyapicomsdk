package com.ekylibre.android.ekyapicom.sdk.jsonclasses.phytos

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class Translations(var fra: String) {
}