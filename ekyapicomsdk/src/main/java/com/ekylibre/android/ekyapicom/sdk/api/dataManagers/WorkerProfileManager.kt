package com.ekylibre.android.ekyapicom.sdk.api.dataManagers

import android.content.Context
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.api.APICommunicationModule
import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.ekylibre.android.ekyapicom.sdk.api.ApiInterface
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.WorkerProfile
import retrofit2.Call
import retrofit2.Response

class WorkerProfileManager(var context: Context, var ekyDbSDK: EkyDbSDK): APICommunicationModule {

    override val errorSharedPrefsKey: String
        get() = "sync_error"

    override fun onResponseCallback(r: Response<APIResponse>) {
        if (r.body() != null && r.isSuccessful) {
            val profile: WorkerProfile = r.body()!! as WorkerProfile
            ekyDbSDK.storeAccountWorkerId(profile.worker_id)
            Utils.saveInSharedPreferences("last_profile_sync", System.currentTimeMillis().toString(), context)
            Utils.saveInSharedPreferences("eky_worker_id", profile.worker_id, context)
        }
    }

    fun pull() {
        val response = ApiInterface.get(context)?.getWorkerProfileAsync()
        val apiUrl = Utils.readStringInSharedPreferences("api_url", context)
        response?.let { enqueueResponse(apiUrl, response as Call<APIResponse>, context) }
    }
}