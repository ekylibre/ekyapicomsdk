package com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions

import com.squareup.moshi.JsonClass
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

@JsonClass(generateAdapter = true)
class InterventionToSend(var provider: Providers, var procedure_name: String,
                         var working_periods_attributes: List<WorkingPeriodsAttributes>,
                         var doers_attributes: List<ParamAttributes>?,
                         var inputs_attributes: List<ParamAttributes>?,
                         var outputs_attributes: List<ParamAttributes>?,
                         var targets_attributes: List<ParamAttributes>?,
                         var tools_attributes: List<ParamAttributes>?,
                         var group_parameters_attributes: List<GroupParamAttributes>?) {

    @Transient
    var id: Long = 0

    fun toJson(): String {
        return generateMoshiAdapter().toJson(this)
    }

    fun getPreview(): String {
        return if (this.group_parameters_attributes.isNullOrEmpty())
            if (this.targets_attributes.isNullOrEmpty())
                if (this.inputs_attributes.isNullOrEmpty())
                    if (this.outputs_attributes.isNullOrEmpty())
                        if (this.tools_attributes.isNullOrEmpty())
                            if (this.doers_attributes.isNullOrEmpty())
                                ""
                            else this.doers_attributes!![0].getPreview()
                        else this.tools_attributes!![0].getPreview()
                    else this.outputs_attributes!![0].getPreview()
                else this.inputs_attributes!![0].getPreview()
            else this.targets_attributes!![0].getPreview()
        else this.group_parameters_attributes!![0].getPreview()
    }

    companion object {
        private fun generateMoshiAdapter(): InterventionToSendJsonAdapter {
            val moshi = Moshi.Builder()
                    .add(KotlinJsonAdapterFactory())
                    .build()
            return InterventionToSendJsonAdapter(moshi)
        }

        fun fromJson(i: String, oldId: Long): InterventionToSend? {
            val inter = generateMoshiAdapter().fromJson(i)
            inter?.let { it.id = oldId }
            return inter
        }
    }
}