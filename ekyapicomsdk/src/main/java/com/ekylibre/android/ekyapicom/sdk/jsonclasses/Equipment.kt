package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.EquipmentDB
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class Equipment(var id: Long, var name: String, var number: String, var variety: String, var born_at: Date, var abilities: MutableList<String>, var work_number: String?, var has_hour_counter: Boolean): InterItem {

    @Transient
    private var hour = 0.0

    fun adaptForDb(): EquipmentDB {
        return EquipmentDB(id, name, number, variety, born_at.time, abilities.joinToString(), if (work_number != null) work_number!! else "", has_hour_counter)
    }

    companion object {
        fun createFromEquipmentDB(e: EquipmentDB): Equipment {
            return Equipment(e.e_id, e.name, e.number, e.variety, Date(e.born_at), e.abilities.split(", ").toMutableList(), e.work_number, e.has_hour_counter)
        }
    }

    override fun getProductId(): Long { return this.id }
    override fun getProductName(): String { return this.name }
    override fun setQuantity(q: Double?, unit: String?) {}
    override fun getQuantity(): Double? { return null }
    override fun getUnit(): String? { return null }
    override fun getHours(): Double? { return this.hour }
    override fun getItemVariety(): String { return this.variety }
    override fun getItemAbilities(): List<String> { return this.abilities }
    override fun getStartDate(): Date? { return this.born_at }
    override fun getEndDate(): Date? { return null }
    override fun getDerivative(): String? { return null }
    override fun getSurfaceArea(): NetSurfaceArea? { return null }
    override fun getSvgShape(): String? { return null }
}