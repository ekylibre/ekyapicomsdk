package com.ekylibre.android.ekyapicom.sdk.filter

import android.util.Log
import com.ekylibre.android.ekyapicom.sdk.Utils
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.*
import java.util.*
import kotlin.collections.ArrayList

class Filter(rawString: String) {

    private var operator: Operator? = null
    private var childFilter: ArrayList<Filter> = ArrayList()
    var neededCapabilities: ArrayList<String> = ArrayList()

    init {
        var tmpChildString = ""
        var tmpFilterString = ""
        var nbParenthesis = 0
        var isInChildBlock = false

        for (i in rawString.indices) {
            when (rawString[i]) {
                '(' -> {
                    nbParenthesis += 1
                    if ((i == 0) || (rawString[i - 1] == ' ' && !isInChildBlock))
                        isInChildBlock = true
                }
                ')' -> nbParenthesis -= 1
            }

            if (isInChildBlock)
                if (nbParenthesis == 0) {
                    this.childFilter.add(Filter(sanitizeFilterString(tmpChildString)))
                    tmpFilterString = sanitizeFilterString(tmpFilterString)
                    isInChildBlock = false
                    tmpChildString = ""
                } else
                    tmpChildString = tmpChildString.plus(rawString[i])
            else
                tmpFilterString = tmpFilterString.plus(rawString[i])
        }

        tmpFilterString = sanitizeFilterString(tmpFilterString)
        when {
            tmpFilterString.contains(" or ") -> {
                this.neededCapabilities = tmpFilterString.split(" or ") as ArrayList<String>
                this.operator = Operator.OR
            }
            tmpFilterString.contains(" and ") -> {
                this.neededCapabilities = tmpFilterString.split(" and ") as ArrayList<String>
                this.operator = Operator.AND
            }
            else -> this.neededCapabilities = arrayListOf(tmpFilterString)
        }
    }

    private fun sanitizeFilterString(fs: String): String {
        if (fs.isEmpty() || fs.length < 3)
            return fs

        return when {
            fs.substring(0, 3) == "and" -> sanitizeFilterString(fs.substring(3))
            fs.substring(0, 2) == "or" -> sanitizeFilterString(fs.substring(2))
            fs.substring(0, 1) == "(" -> sanitizeFilterString(fs.substring(1))
            fs.substring(0, 1) == " " -> sanitizeFilterString(fs.substring(1))
            fs.substring(fs.length -1) == " " -> sanitizeFilterString(fs.substring(0, fs.length -1))
            fs.substring(fs.length -1) == ")" -> if (((fs.filter { it == ')' }.count()) + (fs.filter { it == '(' }.count())).rem(2) != 0) sanitizeFilterString(fs.substring(0, fs.length -1)) else fs
            fs.substring(fs.length -3) == "and" -> fs.substring(0, fs.length -3)
            fs.substring(fs.length -2) == "or" -> fs.substring(0, fs.length -2)
            else -> fs
        }
    }


    private fun match(item: InterItem): Boolean {
        return if (this.childFilter.isEmpty())
            matchWithFilterString(item)
        else {
            val booleans = ArrayList<Boolean>()
            this.childFilter.map { booleans.add(it.match(item)) }
            matchWithFilterString(item) && interpret(booleans)
        }
    }

    private fun matchWithFilterString(item: InterItem): Boolean {
        if (this.neededCapabilities.isNullOrEmpty())
            return true

        val booleans = ArrayList<Boolean>()

        this.neededCapabilities.map {
            if (it.contains(" ")) {
                when (it.split(" ")[0]) {
                    "is" -> booleans.add(checkItemVariety(item, it.split(" ")[1]))
                    "can" -> booleans.add(checkItemAbilities(item, it.split(" ")[1]))
                    "derives" -> booleans.add(checkItemDerivative(item, it.split(" ")[2]))
                    else -> {}
                }
            }
        }

        return interpret(booleans)
    }

    private fun checkItemDerivative(item: InterItem, s: String): Boolean {
        return (item.getDerivative() ?: "").contains(s) || (Onthology.getFamily(item.getDerivative() ?: "").contains(s))
    }

    private fun checkItemVariety(item: InterItem, s: String): Boolean {
        return checkVariety(item.getItemVariety() ?: "", s)
    }

    private fun checkVariety(itemVariety: String, s: String): Boolean {
        if (itemVariety.contains(s))
            return true
        if (itemVariety.contains(Onthology.getFamily(itemVariety))) // if this check is succesfull, but the precedent isn't, that means that we are already at the hightest ontology level, so the variety does not match with the filter
            return false

        return checkVariety(Onthology.getFamily(itemVariety), s)
    }

    private fun checkItemAbilities(item: InterItem, s: String): Boolean {
        return (item.getItemAbilities().contains(s) || checkWithOnthology(item.getItemAbilities(), s))
    }

    private fun checkWithOnthology(itemAbilities: List<String>, filterPart: String): Boolean {
        val s = Regex("\\(.*\\)").find(filterPart)?.value
        if (s.isNullOrEmpty())
            return false

        val familyFromItem = filterPart.substringBefore("(") + "(" + Onthology.getFamily(s.substring(1, s.length -1)) + ")"
        val groupFromItem = filterPart.substringBefore("(") + "(" + Onthology.getFamily(s.substring(1, s.length -1)) + ")"
        val groupFromFamily = filterPart.substringBefore("(") + "(" + Onthology.getFamily(Onthology.getFamily(s.substring(1, s.length -1))) + ")"
        return itemAbilities.contains(familyFromItem) || itemAbilities.contains(groupFromItem) || itemAbilities.contains(groupFromFamily)
    }

    private fun interpret(booleans: ArrayList<Boolean>): Boolean {
        if (booleans.isNullOrEmpty())
            return true

        var res = booleans[0]
        when (this.operator) {
            Operator.AND -> booleans.map { res = res && it }
            Operator.OR -> booleans.map { res = res || it }
        }
        return res
    }
    
    private fun matchDate(i: InterItem, start: Calendar?, end: Calendar?): Boolean {
        val sc = Utils.createCalendar(i.getStartDate()) ?: return true
        val ec = Utils.createCalendar(i.getEndDate()) ?: return true

        start?.let {
            end?.let {
                return (sc <= start && ec >= start) && (sc <= end && ec >= end)
            }
            return (sc >= start && ec >= start)
        }
        return end == null
    }

    companion object {
        fun filterList(list: List<InterItem>, filterString: String?, start: Calendar?, end: Calendar?): ArrayList<InterItem> {
            if (filterString.isNullOrBlank())
                return list as ArrayList<InterItem>

            val resList = ArrayList<InterItem>()

            val filter = Filter(filterString)
            list.map {
                if (filter.match(it) && filter.matchDate(it, start, end))
                    resList.add(it)
            }
            return resList
        }

        fun filterList(list: List<InterItem>, filterString: String?): ArrayList<InterItem> {
            return filterList(list, filterString, null, null)
        }

        fun match(item: InterItem, filterString: String?): Boolean {
            return filterList(listOf(item), filterString).contains(item)
        }
    }
}