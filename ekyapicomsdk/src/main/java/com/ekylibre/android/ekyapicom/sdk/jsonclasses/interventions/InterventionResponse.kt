package com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions

import com.ekylibre.android.ekyapicom.sdk.SyncState
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionParameterDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionWithParameterAndProductDB
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionParameterWithProductDB
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
open class InterventionResponse (var id: Long, var nature: String, var procedure_name: String, var number: Long, var name: String, var started_at: Date, var stopped_at: Date,
                                 var description: String?, var state: String?, var working_duration: Long?, var request_intervention_id: Long?, var costing: Costing, var parameters: MutableList<Parameter>) {

    fun adaptForDb(accountId: Long): InterventionDB {
        return InterventionDB(id, nature, procedure_name, number, name, started_at.time, stopped_at.time, description ?: "", costing.total, accountId, SyncState.REMOTE.ordinal)
    }

    fun getTargets(): String {
        return getParameterIdsByRole("target").joinToString(", ")
    }

    fun getTargetsQuantity(): Int {
        return getParamQuantity("target")
    }

    fun getParamQuantity(role: String): Int {
        return getParameterIdsByRole(role).size
    }

    fun getInputs(): String {
        return getParameterIdsByRole("input").joinToString(", ")
    }

    private fun getParameterIdsByRole(role: String): ArrayList<Long> {
        var res = ArrayList<Long>()
        for (p in parameters) {
            if (p.role == role)
                res.add(p.product_id)
        }

        return res
    }

    override fun toString(): String {
        var tmp = "Intervention: ${id}, nature: $nature, procedure: $procedure_name, number: $number, name: $name, start date: $started_at, stop date: $stopped_at, description: $description, parameters: \n"
        for (p: Parameter in parameters)
            tmp = "$tmp - ${parameters.indexOf(p)}. $p"

        return tmp
    }

    companion object {
        fun createFromInterventionDB(i: InterventionDB): InterventionResponse {
            return InterventionResponse(i.i_id, i.nature, i.procedure, i.number, i.name, Date(i.startDate), Date(i.stopDate), i.description, null, null, null, Costing(i.totalCost, 0.0, 0.0, 0.0, 0.0), mutableListOf())
        }

        fun createFromInterventionDB(i: InterventionDB, params: List<InterventionParameterWithProductDB>): InterventionResponse {
            return InterventionResponse(i.i_id, i.nature, i.procedure, i.number, i.name, Date(i.startDate), Date(i.stopDate), i.description, null, null, null, Costing(i.totalCost, 0.0, 0.0, 0.0, 0.0), params.map { p -> Parameter.createFromParameterDB(p.parameter) }.toMutableList())
        }

        fun createFromInterventionDB(i: InterventionWithParameterAndProductDB): InterventionResponse {
            return createFromInterventionDB(i.intervention, i.interventionParameters)
        }
    }
}
