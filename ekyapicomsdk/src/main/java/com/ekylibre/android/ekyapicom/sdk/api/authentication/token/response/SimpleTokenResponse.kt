package com.ekylibre.android.ekyapicom.sdk.api.authentication.token.response

import com.ekylibre.android.ekyapicom.sdk.api.APIResponse
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class SimpleTokenResponse(var token: String): APIResponse {
}