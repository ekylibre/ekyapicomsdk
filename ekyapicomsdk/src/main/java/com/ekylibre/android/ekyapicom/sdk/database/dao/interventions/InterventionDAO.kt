package com.ekylibre.android.ekyapicom.sdk.database.dao.interventions

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionDB

@Dao
interface InterventionDAO {
    @Query("SELECT * FROM interventiondb")
    fun getAll(): List<InterventionDB>

    @Query("SELECT * FROM interventiondb WHERE i_account_id == (:accountId)")
    fun loadAllByAccount(accountId: Long): List<InterventionDB>

    @Query("SELECT * FROM interventiondb WHERE i_id IN (:interventionIds)")
    fun loadAllByIds(interventionIds: IntArray): List<InterventionDB>

    @Query("SELECT * FROM interventiondb WHERE i_id == (:id)")
    fun get(id: Long): InterventionDB

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg interventionDB: InterventionDB): List<Long>

    @Delete
    fun delete(interventionDB: InterventionDB)

    @Query("DELETE FROM interventiondb")
    fun deleteAll()
}