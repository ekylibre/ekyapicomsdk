package com.ekylibre.android.ekyapicom.sdk.database.dao.lexicon

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.PhytoDB

@Dao
interface PhytoDAO {
    @Query("SELECT * FROM phytodb")
    fun getAll(): List<PhytoDB>

    @Query("SELECT * FROM phytodb WHERE phyto_id IN (:phytoIds)")
    fun loadAllByIds(phytoIds: LongArray): List<PhytoDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg phytoDB: PhytoDB)

    @Delete
    fun delete(phytoDb: PhytoDB)
}