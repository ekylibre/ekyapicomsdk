package com.ekylibre.android.ekyapicom.sdk

import android.content.Context
import android.util.Log

enum class ApiComError {
    NO_ADDRESS_ASSOCIATED_WITH_HOST_NAME {
        override val id = R.string.no_address_associated_with_hostname
    },
    WRONG_USERNAME_OR_PASSWORD {
        override val id = R.string.wrong_username_or_password
    },
    UNKNOWN_ERROR {
        override val id = R.string.unknown_error
    };

    abstract val id: Int
    fun get(context: Context): String = context.getString(id)

    companion object {
        fun getApiComErrorFromMessage(message: String): ApiComError {
            when {
                message.contains("No address associated with hostname") -> return NO_ADDRESS_ASSOCIATED_WITH_HOST_NAME
                message.contains("Unauthorized") -> return WRONG_USERNAME_OR_PASSWORD
            }
            Log.e("UNKNOWN_ERROR", message)
            return UNKNOWN_ERROR
        }
    }
}