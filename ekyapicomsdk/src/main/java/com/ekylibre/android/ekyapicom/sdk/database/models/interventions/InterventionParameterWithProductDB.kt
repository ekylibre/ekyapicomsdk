package com.ekylibre.android.ekyapicom.sdk.database.models.interventions

import androidx.room.Embedded
import androidx.room.Relation
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem

class InterventionParameterWithProductDB(var parameter: InterventionParameterDB, var interventionProduct: InterItem?) {

}