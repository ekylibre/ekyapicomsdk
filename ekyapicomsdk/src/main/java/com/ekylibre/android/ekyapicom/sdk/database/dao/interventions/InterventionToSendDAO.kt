package com.ekylibre.android.ekyapicom.sdk.database.dao.interventions

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionToSendDB

@Dao
interface InterventionToSendDAO {

    @Query("SELECT * FROM interventiontosenddb")
    fun getAll(): List<InterventionToSendDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg its: InterventionToSendDB)

    @Delete
    fun delete(its: InterventionToSendDB)
}