package com.ekylibre.android.ekyapicom.sdk.api.authentication

import android.content.Context
import android.util.Log
import com.ekylibre.android.ekyapicom.sdk.api.authentication.token.manager.SimpleTokenManager
import com.ekylibre.android.ekyapicom.sdk.api.authentication.token.manager.TokenManager
import com.ekylibre.android.ekyapicom.sdk.api.dataManagers.PlantManager
import com.ekylibre.android.ekyapicom.sdk.api.dataManagers.WorkerProfileManager
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import kotlin.concurrent.thread

class SimpleAuthModule(
    override var apiUrl: String,
    override var context: Context,
    var ekyDbSDK: EkyDbSDK
) : AuthModuleInterface {

    override var accessToken = ""
    override var username = ""
    override var password = ""
    override var tokenManager: TokenManager = SimpleTokenManager(apiUrl, context, ekyDbSDK)

    override fun init() {
        val sharedPreferences = context.getSharedPreferences(context.packageName, 0)
        val accessToken = sharedPreferences.getString("access_token", "")!!
        val username = sharedPreferences.getString("username", "")!!
        val apiUrl = sharedPreferences.getString("api_url", "")!!

        if ((accessToken == "") || ((username == this.username) && (apiUrl == this.apiUrl)))
            this.create()
        else
            this.start()
    }

    override fun start() {
        Log.e("Step: ", "Refreshing access token...")
        tokenManager.refreshAccessToken(null)
    }

    override fun create() {
        Log.e("Step: ", "Creating access token...")
        tokenManager.generateAccessToken()
    }
}