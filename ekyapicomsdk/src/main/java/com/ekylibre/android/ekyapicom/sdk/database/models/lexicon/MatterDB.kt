package com.ekylibre.android.ekyapicom.sdk.database.models.lexicon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MatterDB (
    @PrimaryKey val m_id: Long,
    @ColumnInfo(name = "m_name") val name: String,
    @ColumnInfo(name = "m_number") val number: String,
    @ColumnInfo(name = "m_population") val population: String,
    @ColumnInfo(name = "m_variety") val variety: String,
    @ColumnInfo(name = "m_abilities") val abilities: String,
    @ColumnInfo(name = "m_born_at") val born_at: Long,
    @ColumnInfo(name = "m_derivative_of") val derivative_of: String,
    @ColumnInfo(name = "m_variant_unit_name") val unit_name: String,
    @ColumnInfo(name = "m_container_name") val container_name: String
)