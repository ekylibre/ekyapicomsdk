package com.ekylibre.android.ekyapicom.sdk.jsonclasses.interventions

import com.ekylibre.android.ekyapicom.sdk.database.models.interventions.InterventionProductDB
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.InterItem
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
class Product(var id: Long, var name: String) {

    fun adaptForDb(paramId: Long): InterventionProductDB {
        return InterventionProductDB(paramId, name, id)
    }

    override fun toString(): String {
        return "Product: ${id}, name: $name"
    }

    companion object {
        fun createFromProductDB(p: InterventionProductDB): Product {
            return Product(p.ekyProdId, p.name)
        }
    }
}