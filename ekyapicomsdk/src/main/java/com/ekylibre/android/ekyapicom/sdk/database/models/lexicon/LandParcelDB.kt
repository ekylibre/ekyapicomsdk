package com.ekylibre.android.ekyapicom.sdk.database.models.lexicon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class LandParcelDB (
    @PrimaryKey val lp_id: Long,
    @ColumnInfo(name = "lp_name") val name: String,
    @ColumnInfo(name = "lp_net_surface_area") val netSurfaceArea: String,
    @ColumnInfo(name = "lp_born_at") val born_at: Long,
    @ColumnInfo(name = "lp_dead_at") val dead_at: Long,
    @ColumnInfo(name = "lp_variety") val variety: String,
    @ColumnInfo(name = "lp_production_started_on") val productionStartedOn: String,
    @ColumnInfo(name = "lp_production_stopped_on") val productionStopped: String,
    @ColumnInfo(name = "lp_shape_svg") val shapeSvg: String
)