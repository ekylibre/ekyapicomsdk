package com.ekylibre.android.ekyapicom.sdk.database.models.lexicon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PhytoDB (
    @PrimaryKey val phyto_id: Long,
    @ColumnInfo(name = "phyto_name") val name: String,
    @ColumnInfo(name = "phyto_reference_name") val referenceName: String,
    @ColumnInfo(name = "phyto_other_names") val otherNames: String,
    @ColumnInfo(name = "phyto_natures") val natures: String,
    @ColumnInfo(name = "phyto_active_compounds") val activeCompounds: String,
    @ColumnInfo(name = "phyto_maaid") val maaid: String,
    @ColumnInfo(name = "phyto_mix_category_codes") val mixCategoryCodes: String,
    @ColumnInfo(name = "phyto_in_field_reentry_delay") val inFieldReentryDelay: Long,
    @ColumnInfo(name = "phyto_state") val state: String,
    @ColumnInfo(name = "phyto_started_on") val startedOn: String,
    @ColumnInfo(name = "phyto_stopped_on") val stoppedOn: String,
    @ColumnInfo(name = "phyto_allowed_mentions") val allowedMentions: String,
    @ColumnInfo(name = "phyto_restricted_mentions") val restrictedMentions: String,
    @ColumnInfo(name = "phyto_operator_protection_mentions") val operatorProtectionMentions: String,
    @ColumnInfo(name = "phyto_firm_name") val firmName: String,
    @ColumnInfo(name = "phyto_product_type") val productType: String,
    @ColumnInfo(name = "phyto_record_checksum") val recordChecksum: Long,
)