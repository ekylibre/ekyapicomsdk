package com.ekylibre.android.ekyapicom.sdk

import android.content.Context
import android.util.Log
import com.ekylibre.android.ekyapicom.sdk.jsonclasses.procedures.Procedure
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class Utils {

    companion object {
        fun saveInSharedPreferences(keysValues: Map<String, Any>, context: Context) {
            for ((k, v) in keysValues) {
                saveInSharedPreferences(k, v, context)
            }
        }

        fun saveInSharedPreferences(key: String, value: Any, context: Context) {
            val sharedPreferences = context.getSharedPreferences(context.packageName, 0)
            val prefsEditor = sharedPreferences!!.edit()

            when (value) {
                is String -> prefsEditor.putString(key, value)
                is Int -> prefsEditor.putInt(key, value)
                is Boolean -> prefsEditor.putBoolean(key, value)
                is Long -> prefsEditor.putLong(key, value)
            }

            Log.e(
                "EkyapicomSDK",
                "Saved value " + value + " in " + context.packageName + " under key " + key
            )

            prefsEditor.apply()
        }

        fun readStringInSharedPreferences(key: String, context: Context): String {
            val sharedPreferences = context.getSharedPreferences(context.packageName, 0)
            return try {
                sharedPreferences!!.getString(key, "")!!
            } catch (e: Exception) {
                ""
            }
        }

        fun readIntInSharedPreferences(key: String, context: Context): Int {
            val sharedPreferences = context.getSharedPreferences(context.packageName, 0)
            return sharedPreferences!!.getInt(key, -1)
        }

        fun readBooleanInSharedPreferences(key: String, context: Context): Boolean {
            val sharedPreferences = context.getSharedPreferences(context.packageName, 0)
            return sharedPreferences!!.getBoolean(key, false)
        }

        fun readLongInSharedPreferences(key: String, context: Context): Long {
            val sharedPreferences = context.getSharedPreferences(context.packageName, 0)
            return sharedPreferences!!.getLong(key, -1)
        }

        fun refreshProcedureFiles(context: Context): List<Procedure> {
            val jsonFilesDirectory = "procedures/json/procedures.json"

            return getJsonProcedure(context, jsonFilesDirectory)
        }

        private fun getJsonProcedure(context: Context, file: String): List<Procedure> {
            val moshi = Moshi.Builder()
                .add(KotlinJsonAdapterFactory())
                .build()

            val listType = Types.newParameterizedType(List::class.java, Procedure::class.java)
            val adapter: JsonAdapter<List<Procedure>> = moshi.adapter(listType)

            val procedureJson = context.assets.open(file).bufferedReader().use{ it.readText()}

            return adapter.fromJson(procedureJson)!!
        }

        fun getTodaysDate(): String {
            return SimpleDateFormat("dd-MM-yyy").format(Calendar.getInstance().time) + "T" + SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().time)
        }

        fun createCalendar(d: Date?): Calendar? {
            d?.let {
                val c = Calendar.getInstance()
                c.time = d
                return c
            }

            return null
        }

        fun createDate(s: String?): Date? {
            s?.let {
                val splitedString = s.split("-")
                if (splitedString.size == 3) {
                    return Date(splitedString[0].toInt(), splitedString[1].toInt(), splitedString[2].toInt())
                }
            }

            return null
        }
    }
}