package com.ekylibre.android.ekyapicom.sdk.database.dao.lexicon

import androidx.room.*
import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.AnimalDB

@Dao
interface AnimalDAO {
    @Query("SELECT * FROM animaldb")
    fun getAll(): List<AnimalDB>

    @Query("SELECT * FROM animaldb WHERE a_variety LIKE :variety")
    fun getbyVariety(variety: String): List<AnimalDB>

    @Query("SELECT * FROM animaldb WHERE a_id IN (:ids)")
    fun loadAllByIds(ids: LongArray): List<AnimalDB>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg animalDB: AnimalDB)

    @Delete
    fun delete(animalDB: AnimalDB)
}