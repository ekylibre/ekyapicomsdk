package com.ekylibre.android.ekyapicom.sdk.jsonclasses

import com.ekylibre.android.ekyapicom.sdk.database.models.lexicon.WorkerDB
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
class Worker(var id: Long, var name: String, var number: String, var work_number: String?, var born_at: Date, var variety: String, var abilities: MutableList<String>): InterItem {

    fun adaptForDb(): WorkerDB {
        return WorkerDB(id, name, number, if (work_number != null) work_number!! else "", born_at.time, variety, abilities.joinToString())
    }

    companion object {
        fun createFromWorkerDB(w: WorkerDB): Worker {
            return Worker(w.w_id, w.name, w.number, w.work_number, Date(w.born_at), w.variety, w.abilities.split(", ").toMutableList())
        }
    }

    override fun getProductId(): Long {
        return this.id
    }

    override fun getProductName(): String {
        return this.name
    }

    override fun setQuantity(q: Double?, unit: String?) {}
    override fun getQuantity(): Double? { return null }
    override fun getUnit(): String? { return null }
    override fun getHours(): Double? { return null }
    override fun getItemVariety(): String { return this.variety }
    override fun getItemAbilities(): List<String> { return this.abilities }
    override fun getStartDate(): Date? { return this.born_at }
    override fun getEndDate(): Date? { return null }
    override fun getDerivative(): String? { return null }
    override fun getSurfaceArea(): NetSurfaceArea? { return null }

    override fun getSvgShape(): String? { return null }
}