package com.ekylibre.android.ekyapicom.sdk.filter

class Onthology() {
    companion object {

        var onthologyMap = @onthology


        fun getFamily(name: String): String {
            val map = onthologyMap
            map.keys.map { if (map[it]!!.contains(name)) return it }

            return name
        }

        fun getTopFamily(name: String): String {
            if (name.contains(getFamily(name)))
                return name

            return getTopFamily(getFamily(name))
        }
    }
}