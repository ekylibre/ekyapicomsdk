def extract_value_from_XML(l, k):
    x = l.split(k + "=")
    if (len(x) > 1):
        y = x[1].split()
        return y[0].split("\"")[1]
    else:
        return ""

def add_line_to_dict(d, l):
    name = extract_value_from_XML(line, "name")
    parent = extract_value_from_XML(line, "parent")
    variety = extract_value_from_XML(line, "variety")
    if (variety != "" and parent == ""):
        parent = variety

    if (parent == "" and name != "" and name not in d):
        d[name] = []
    if (parent != "" and parent != name):
        if (parent not in d):
            d[parent] = [name]
        else:
            d[parent].append(name)

def build_list_string(k, l):
    s = "\", \"".join(l)
    return '            "' + k + '" to arrayListOf("' + s + '"),\n'

def build_map_string(d):
    s = 'mapOf(\n"land_parcel" to arrayListOf("cultivation"),\n'
    for key in d:
        s += build_list_string(key, d[key])
    s = s[:len(s) -3] + ")\n        )"
    return s

file1 = open('db.xml', 'r')
lines = file1.readlines()

in_block = False
res_dict = {}

for line in lines:
    if ("<nomenclature name=\"varieties\" translateable=\"true\">" in line or '<nomenclature name="product_natures" translateable="true">' in line):
        in_block = True

    if ("</nomenclature>" in line and in_block):
        in_block = False

    if (in_block and "<item" in line):
        add_line_to_dict(res_dict, line)

base_file = open('Onthology.kt', 'r')
base_lines = base_file.readlines()
f = open("Onthology.kt", "w")

for l in base_lines:
    if ("@onthology" in l):
        f.write(l.replace("@onthology", build_map_string(res_dict)))
        #build_map_string(res_dict)
    else:
        f.write(l)
f.close()

