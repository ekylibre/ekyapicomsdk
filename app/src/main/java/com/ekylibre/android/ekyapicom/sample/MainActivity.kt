package com.ekylibre.android.ekyapicom.sample

import android.content.SharedPreferences
import com.ekylibre.android.ekyapicom.sdk.EkyAPIComSDK
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import com.ekylibre.android.ekyapicom.sdk.database.EkyDbSDK
import com.ekylibre.android.ekyapicom.sdk.filter.Filter
import com.ekylibre.android.ekyapicom.sdk.filter.Onthology
import java.util.*

class MainActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

    lateinit var ekyAPIComSDK: EkyAPIComSDK
    private var sharedPreferences: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        applicationContext.getSharedPreferences(applicationContext.packageName, 0).registerOnSharedPreferenceChangeListener(this)

        sharedPreferences = applicationContext.getSharedPreferences(applicationContext.packageName, 0)

        initEkyDbSDK()
        initEkyAPIComSDK()
        initUi()
        initOnClickListeners()
        retrieveDataInStorage()
    }

    private fun initEkyDbSDK() {
    }

    private fun initEkyAPIComSDK() {
        //ekyAPIComSDK = EkyAPIComSDK(NetworkConstant.BASE_URL, NetworkConstant.CLIENT_ID, NetworkConstant.CLIENT_SECRET, applicationContext)
        ekyAPIComSDK = EkyAPIComSDK(NetworkConstant.BASE_URL, this)

        if (EkyAPIComSDK.isUserLogged(applicationContext)) {
            ekyAPIComSDK.init()
            findViewById<Button>(R.id.destroy_button).isClickable = true
        }
    }

    private fun initUi() {
        findViewById<EditText>(R.id.sample_username).setText(NetworkConstant.DEFAULT_NATAIS_USERNAME)
        findViewById<EditText>(R.id.sample_password).setText(NetworkConstant.DEFAULT_NATAIS_PASSWORD)
    }

    private fun retrieveDataInStorage() {
        val accessToken = sharedPreferences!!.getString("access_token", "No Access Token stored")!!
        val refreshToken = sharedPreferences!!.getString("refresh_token", "No Refresh Token stored")!!
        val apiUrl = sharedPreferences!!.getString("api_url", "No API URL stored")!!

        findViewById<EditText>(R.id.access_token_et).setText(accessToken)
        findViewById<EditText>(R.id.refresh_token_et).setText(refreshToken)
        findViewById<EditText>(R.id.api_url_et).setText(apiUrl)
    }

    private fun initOnClickListeners() {
        findViewById<Button>(R.id.getTokensButton).setOnClickListener {
            val username = findViewById<EditText>(R.id.sample_username).text.toString()
            val password = findViewById<EditText>(R.id.sample_password).text.toString()
            ekyAPIComSDK.setCredentials(username, password)
            ekyAPIComSDK.init()
        }

        findViewById<Button>(R.id.destroy_button).setOnClickListener {
            ekyAPIComSDK.destroy()
        }

        // ------------------ API CALLS BUTTONS ------------------ //
        findViewById<Button>(R.id.get_inter_button).setOnClickListener {
            ekyAPIComSDK.getInterventionsFromAPI()
        }

        findViewById<Button>(R.id.get_plants_button).setOnClickListener {
            ekyAPIComSDK.getPlantsFromAPI()
        }

        findViewById<Button>(R.id.get_workers_button).setOnClickListener {
            ekyAPIComSDK.getWorkersFromAPI()
        }

        findViewById<Button>(R.id.get_equipments_button).setOnClickListener {
            ekyAPIComSDK.getEquipmentsFromAPI()
        }

        findViewById<Button>(R.id.get_land_parcels_button).setOnClickListener {
            ekyAPIComSDK.getLandParcelsFromAPI()
        }

        findViewById<Button>(R.id.get_building_divisions_button).setOnClickListener {
            ekyAPIComSDK.getBuildingDivisionsFromAPI()
        }

        findViewById<Button>(R.id.get_matters_button).setOnClickListener {
            ekyAPIComSDK.getMattersFromAPI()
        }

        findViewById<Button>(R.id.get_phytos_button).setOnClickListener {
            ekyAPIComSDK.getPhytosFromAPI()
        }

        findViewById<Button>(R.id.get_phyto_usages_button).setOnClickListener {
            ekyAPIComSDK.getPhytoUsagesFromAPI()
        }

        findViewById<Button>(R.id.get_animals_button).setOnClickListener {
            ekyAPIComSDK.getAnimalsFromAPI()
        }

        // ------------------ DB CALLS BUTTONS ------------------ //
        findViewById<Button>(R.id.get_inter_db_button).setOnClickListener {
            ekyAPIComSDK.getRemoteInterventionsFromDB()
        }

        findViewById<Button>(R.id.get_plants_db_button).setOnClickListener {
            ekyAPIComSDK.getPlantsFromDB()
        }

        findViewById<Button>(R.id.get_workers_db_button).setOnClickListener {
            ekyAPIComSDK.getWorkersFromDB()
        }

        findViewById<Button>(R.id.get_equipments_db_button).setOnClickListener {
            ekyAPIComSDK.getEquipmentsFromDB()
        }

        findViewById<Button>(R.id.get_land_parcels_db_button).setOnClickListener {
            ekyAPIComSDK.getLandParcelsFromDB()
        }

        findViewById<Button>(R.id.get_building_divisions_db_button).setOnClickListener {
            ekyAPIComSDK.getBuildingDivisionsFromDB()
        }

        findViewById<Button>(R.id.get_matters_db_button).setOnClickListener {
            ekyAPIComSDK.getMattersFromDB()
        }

        findViewById<Button>(R.id.get_phytos_db_button).setOnClickListener {
            ekyAPIComSDK.getPhytosFromDB()
        }

        findViewById<Button>(R.id.get_phyto_usages_db_button).setOnClickListener {
            ekyAPIComSDK.getPhytoUsagesFromDB()
        }

        findViewById<Button>(R.id.get_animals_db_button).setOnClickListener {
            ekyAPIComSDK.getAnimalsFromDB()
        }

        // -------------- FIND BY ID BTNS -------------------//
        findViewById<Button>(R.id.get_by_id_btn).setOnClickListener {
            val test = Filter.filterList(ekyAPIComSDK.getWorkersFromDB(), findViewById<EditText>(R.id.id_et).text.toString(), Calendar.getInstance(), null)
//            val test = Onthology.getFamily(findViewById<EditText>(R.id.id_et).text.toString())
            Log.e("RESULT", test.toString())
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == "last_interventions_sync") {
            // do stuff
        }

        if (key == "access_token")
            retrieveDataInStorage()
    }
}
